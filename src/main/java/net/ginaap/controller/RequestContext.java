package net.ginaap.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.servlets.GinaServlet;

/**
 * 
 * RequestContext contains contextual information of a request which is passed
 * along the call stack from initial servlet to renderer and to components
 * 
 * @author lennart.benoot@mincko.com
 * 
 */
public class RequestContext {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private String[] path;
	private String originalPath;
	private String language;
	private String pageName;

	public RequestContext(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.originalPath = request.getParameter( GinaServlet.PARAMETER_PATH);		
		Object langAttr = request.getAttribute(GinaServlet.LANGUAGE_PATH);
		
		if ( this.originalPath != null ) { // can be null for example when FormServlet is called
			this.path = originalPath.split("/");
			
			// length = 3 when url is /en/home. Other cases such as /index.html to be finetuned, but also: /subfolder/index.hmtl
			if (this.path.length == 3) { 
				this.language = this.path[ 1];
				this.pageName = this.path[ 2];
			}
		} else if (langAttr != null) {
			this.language = langAttr.toString();
		}
	}
		
	public String getOriginalPath() {
		return originalPath;
	}

	public void setOriginalPath(String originalPath) {
		this.originalPath = originalPath;
	}

	public void setLanguage(String language) {
		this.language = language;
	}


	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getLanguage() {
		return language;
	}

	public String getPageName() {
		return pageName;
	}
	public boolean isLocalhost() {
		return request.getLocalPort() == 8888;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public void parsePath(String path) {
		this.path = path.split("/");
	}

	public String[] getPath() {
		return path;
	}

	public void setPath(String[] path) {
		this.path = path;
	}

}
