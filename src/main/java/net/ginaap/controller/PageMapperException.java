package net.ginaap.controller;

public class PageMapperException extends Exception {
	
	  public PageMapperException() {
	  }

	  public PageMapperException(String msg) {
	    super(msg);
	  }
	  
	  public PageMapperException(String msg, Exception e) {
		    super(msg, e);
	   }

}
