package net.ginaap.controller.servlets;

import java.io.*;
import java.util.HashMap;
import java.util.regex.Pattern;

import javax.servlet.*;
import javax.servlet.http.*;

import net.ginaap.controller.PageMapper;
import net.ginaap.controller.PageMapperException;
import net.ginaap.controller.RequestContext;
import net.ginaapp.model.Page;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.Renderer;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.CacheService;

/**
* 
* This servlet handles all incoming http request (apart from the static requests).
* 
* @author lennart.benoot@mincko.com
*/
public class GinaServlet extends HttpServlet {

	public static final String PARAMETER_PATH = "path";
	public static final String LANGUAGE_PATH = "lang";
	private static final long serialVersionUID = 1L;
	private static HashMap<String,Long> requestTimeLog= new HashMap<String,Long>();
	
	private static final Pattern p = Pattern.compile("/_ah/.*");

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			// Prepare RequestContext
			RequestContext requestContext = new RequestContext(request, response);

			// Get Page
			Page page= PageMapper.map(requestContext);
			
			
			// Try to get content from cache
			String content = CacheService.get( requestContext.getLanguage() + requestContext.getPageName());
			
			// If not in cache, render
			if (reRender(requestContext, content)) {
				content = Renderer.renderPage(requestContext, page);
				CacheService.put(requestContext.getLanguage() + requestContext.getPageName(), content);
			}
			
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write( content);
			
		} catch (RenderException e) {
			
			response.getWriter().write(e.getMessage());
			
		} catch (PageMapperException e) {

			response.getWriter().write(e.getMessage());
		}

	}

	/**
	 * There's a couple of reason why we should re render.
	 * 
	 * @param content
	 * @return
	 */
	private boolean reRender( RequestContext requestContext, String content) {
		
		// Refersh page if end-user refreshed page within 5 seconds
		Long currentMillis = new Long( System.currentTimeMillis());
		Long previousRequestMillis = requestTimeLog.get( requestContext.getOriginalPath()) ;
		previousRequestMillis = previousRequestMillis != null ? previousRequestMillis : currentMillis; // get rid of null
		requestTimeLog.put( requestContext.getOriginalPath(), System.currentTimeMillis());
		
		if (AuthService.IsLoggedInUserAdmin()) return true; // Force requests from logged in administrators to re-render due to the in-line edit mode		
		if ( ((currentMillis -previousRequestMillis ) < 5000) ) return true;
		if (content == null) return true;
		if (content.contains("Error reading file:")) return true;
		if ("127.0.0.1".equals( requestContext.getRequest().getRemoteHost() )) return true;
		
		return false;
	}
}
