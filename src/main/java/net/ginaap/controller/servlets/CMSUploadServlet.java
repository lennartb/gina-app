package net.ginaap.controller.servlets;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.CMSResponse;
import net.ginaapp.model.Website;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.CacheService;
import net.ginaapp.services.OfyService;
import net.ginaapp.services.TranslationService;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

public class CMSUploadServlet extends HttpServlet {

    private static final long serialVersionUID = 3227688808475532272L;
    private static final Logger log = Logger.getLogger(CMSUploadServlet.class.getName());

    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CMSResponse resp = new CMSResponse();
		String errUnauthorized = "You are not authorized to update this content or your login session could have been discontinued after being idle for a period of time.";
		String errGeneric = "An error occurred while processing content uploads. Please try again or contact support should the problem persist.";
		
		String key = request.getParameter("key");
		String component = request.getParameter("component");
		String imgData = request.getParameter("imgBase64");	
		String language = request.getParameter("language");
		
		RequestContext requestContext = new RequestContext(request, response);
		if (language == null) {
			language = requestContext.getLanguage();			
			if (language == null) {
				Objectify ofy = OfyService.ofy();
				Query<Website> q = ofy.query(Website.class);
				Website website = q.get();
				language = website.getDefaultLanguage();
			}
		}
		
		try {
			errUnauthorized = TranslationService.translate(language, "cms", "cms-image-no-auth");
			errGeneric = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			String username = (user == null) ? null : user.toString(); 
			
			if (username == null || username.isEmpty()) {
				resp.setSuccessful(false);
				resp.setErrorMessage(errUnauthorized);
			} else {
				AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
				GcsFilename gcsFilename = new GcsFilename(appIdentityService.getDefaultGcsBucketName(), key + ".png");							
				GcsFileOptions fileOptions = new GcsFileOptions.Builder()
					.acl("public-read")
					.mimeType("image/png")
					.build();
				
				imgData = imgData.substring(imgData.indexOf(",") + 1);
				GcsOutputChannel outputChannel = gcsService.createOrReplace(gcsFilename, fileOptions);
				outputChannel.write(ByteBuffer.wrap(Base64.decodeBase64(imgData)));
				outputChannel.close();
												
				BlobKey blobKey = blobstoreService.createGsBlobKey("/gs/" + gcsFilename.getBucketName() + "/" + gcsFilename.getObjectName());				
				String blobKeyString = blobKey.getKeyString();
				
				CMSDataService.put(component, language, key, blobKeyString);
				// CacheService.delete(pageKey);  // TODO: Find out why deleting single records from MemCache does not work properly causing caching issues.
	            CacheService.clearAll(); // TODO: Remove after fixing issue above.
	            resp.setSuccessful(true);
	            resp.setData("/serve?blob-key=" + blobKeyString);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "An error occurred while processing CMS uploads for component '" 
					+ component + "' with key '"
					+ key + "' for language '" 
					+ language + "'.",
					e);
			
			resp.setSuccessful(false);
			resp.setErrorMessage(errGeneric);
		}
		
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(resp.toJSON());	    	
    }
}
