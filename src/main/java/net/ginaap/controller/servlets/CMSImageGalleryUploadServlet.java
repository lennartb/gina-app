package net.ginaap.controller.servlets;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.CMSImageGallery;
import net.ginaapp.model.CMSImageGalleryItem;
import net.ginaapp.model.CMSResponse;
import net.ginaapp.model.Website;
import net.ginaapp.renderer.components.core.ImageGallery;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.CacheService;
import net.ginaapp.services.OfyService;
import net.ginaapp.services.TranslationService;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

public class CMSImageGalleryUploadServlet extends HttpServlet {

	private static final long serialVersionUID = -5050776752334450890L;
	private static final Logger log = Logger.getLogger(CMSImageGalleryUploadServlet.class.getName());
	
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		CMSResponse resp = new CMSResponse();
		String errUnauthorized = "You are not authorized to update this content or your login session could have been discontinued after being idle for a period of time.";
		String errGeneric = "An error occurred while processing content uploads. Please try again or contact support should the problem persist.";
		
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(request);
		String key = request.getParameter("key");
		String component = request.getParameter("component");
		String pageKey = request.getParameter("pageKey");
		String additionalAttrs = request.getParameter("additionalAttributes");
		String decodedAdditionalAttrs = URLDecoder.decode(additionalAttrs, "UTF-8"); 
				
		RequestContext requestContext = new RequestContext(request, response);
		String language = request.getParameter("language");
		if (language == null) {
			Objectify ofy = OfyService.ofy();
			Query<Website> q = ofy.query(Website.class);
			Website website = q.get();
			language = website.getDefaultLanguage();
		}
		
		try {
			errUnauthorized = TranslationService.translate(language, "cms", "cms-image-no-auth");
			errGeneric = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			String username = (user == null) ? null : user.toString(); 
			
			if (username == null || username.isEmpty()) {
				resp.setSuccessful(false);
				resp.setErrorMessage(errUnauthorized);
			} else {
				List<BlobKey> blobKeys = blobs.get("upload");
				String[] existingBlobKeys = request.getParameterValues("blobKey");
				String[] isImagesSelected = request.getParameterValues("imageSelected");
				String[] titles = request.getParameterValues("title");
				String[] descriptions = request.getParameterValues("description");
								
				// TODO: Delete blobs from BlobStore if the user has removed it from the gallery
				// or if the blob has been replaced with a new image
				CMSImageGallery gallery = null;
				String json = CMSDataService.get(component, language, key);
				String galleryStr = "";
				try {
					if (json != null && !json.isEmpty()) gallery = CMSImageGallery.fromJson(json);			
					if (gallery != null && gallery.getItems().size() > 0) {
						for (CMSImageGalleryItem item : gallery.getItems()) {
							Boolean isBlobKeyRemoved = true;
							if (existingBlobKeys != null) {
								for (String blobKey : existingBlobKeys) {
									if (blobKey.equals(item.getBlobKey())) {
										isBlobKeyRemoved = false;
										break;
									}
								}
							}
							
							if (isBlobKeyRemoved) blobstoreService.delete(new BlobKey(item.getBlobKey()));
						}
					}
				} catch (Exception ex) {
					log.log(Level.SEVERE, 
							"Could not deserialize JSON to CMSImageGallery. JSON: " + json,
							ex);
				}
				
				gallery = new CMSImageGallery();
				int idx = 0;
				int blobKeyIdx = 0;
				if (isImagesSelected != null) {
					for (String isImageSelected : isImagesSelected) {
						String blobKey = existingBlobKeys[idx];
						String uploadBlobKey = ((blobKeys == null || blobKeys.size() == blobKeyIdx) ? null : blobKeys.get(blobKeyIdx).getKeyString());
						String title = titles[idx];
						String description = descriptions[idx];
						
						if ((isImageSelected == null || isImageSelected.isEmpty()) && blobKey != null && !blobKey.isEmpty()) {
							// If image is not selected but blob key is not empty then update record
							gallery.getItems().add(new CMSImageGalleryItem(blobKey, title, description));
						} else if (isImageSelected != null && !isImageSelected.isEmpty()) {
							// If image is selected then update record
							gallery.getItems().add(new CMSImageGalleryItem(uploadBlobKey, title, description));
							
							//  Delete blobs from BlobStore since the user has replaced old blob with a new image
							if (blobKey != null && !blobKey.isEmpty()) {
								blobstoreService.delete(new BlobKey(blobKey));
							}
						}
						
						// If image is not selected and blob key is empty, then skip
						
						idx++;
						if (!isImageSelected.isEmpty()) blobKeyIdx++;
					}
				}
				
				// Store image gallery information in JSON format
				json = gallery.toJson();
				CMSDataService.put(component, language, key, json);				
				// CacheService.delete(pageKey);  // TODO: Find out why deleting single records from MemCache does not work properly causing caching issues.
				CacheService.clearAll(); // TODO: Remove after fixing issue above.
				resp.setSuccessful(true);
				resp.setData(ImageGallery.getComponentHTML(language, key, pageKey, decodedAdditionalAttrs));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "An error occurred while processing CMS uploads for component '" 
					+ component + "' with key '"
					+ key + "' for language '" 
					+ language + "'.",
					e);
			
			resp.setSuccessful(false);
			resp.setErrorMessage(errGeneric);
			resp.setData(blobstoreService.createUploadUrl("/gallery-upload"));
		}
		
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(resp.toJSON());		
	}	
}
