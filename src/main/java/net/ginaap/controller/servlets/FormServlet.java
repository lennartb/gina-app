package net.ginaap.controller.servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.json.simple.JSONObject;








import net.ginaap.controller.RequestContext;
import net.ginaapp.model.Person;
import net.ginaapp.renderer.components.form.Contact;
import net.ginaapp.renderer.components.form.DocumentDownload;
import net.ginaapp.renderer.components.form.StorePerson;
import net.ginaapp.services.OfyService;

import com.google.appengine.api.NamespaceManager;
import com.googlecode.objectify.Objectify;


public class FormServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -213480730161060360L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		// Prepare RequestContext
		RequestContext requestContext = new RequestContext(request, response);
		
		String domain = requestContext.getRequest().getServerName();
		
		if ( domain.contains( "mincko")) {
		
			( new StorePerson()).performAction( requestContext);
			( new DocumentDownload()).performAction( requestContext);
			
		}

		// for all other domains
		( new Contact()).performAction( requestContext);


	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

}
