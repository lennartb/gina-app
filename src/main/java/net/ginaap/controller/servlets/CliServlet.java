package net.ginaap.controller.servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

import net.ginaapp.cli.CliUtil;
import net.ginaapp.model.Website;
import net.ginaapp.services.CacheService;
import net.ginaapp.services.OfyService;

/**
 * CliServlet let's you create the initial WebsiteObject
 */
public class CliServlet extends HttpServlet {

	private static final String PARAMETER_CMD = "cmd";
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String cmd = request.getParameter( PARAMETER_CMD);
		
		response.setContentType( "text/html");

		try {
			Objectify ofy = OfyService.ofy();
			
			if ( "refresh".equals( cmd)) {
				
				CacheService.clearAll();
				response.getWriter().print("<br><div style=\"font-family:sans-serif\">Website has been refreshed!</div>");
			}
			else if ( "createFromFile".equalsIgnoreCase( cmd)) {
				//create form createFile
				String log= CliUtil.createFromCreateFile();
				response.getWriter().print("<br><div style=\"font-family:sans-serif\">" +log  + "</div>");
				
			}
			else if (cmd != null) 
			{
				// If Webiste object is created, Delete all existing Website object (Approach can be improved)
				if ( cmd.startsWith("Website")) {
					Query<Website> q = ofy.query(Website.class);
					for (Website website: q) {
					    ofy.delete( website);
					}
				}
				
				CliUtil.createObject( cmd);
			}
			
			response.getWriter().print("<br><br><div style=\"font-family:sans-serif\">"+cmd+" successfully executed.<div>");
		}
		catch (Exception e) {
			response.getWriter().print("<br><br><div style=\"font-family:sans-serif\">"+cmd+" could not be executed: "+e.getMessage()+"<div>");
		}
	}
}
