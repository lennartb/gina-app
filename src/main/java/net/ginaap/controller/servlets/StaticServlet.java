package net.ginaap.controller.servlets;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.*;
import javax.servlet.http.*;

import com.google.api.server.spi.response.InternalServerErrorException;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.model.Website;
import net.ginaapp.renderer.ContentRenderer;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.services.TranslationService;

/**
 * StaticServlet serves static content of a website by getting it from google
 * drive.
 * Html files will be scanned for GinaComponents and rendered.
 * 
 * @author lennart.benoot@minkco.com
 */
public class StaticServlet extends HttpServlet {
	private static final String CONTENT_TYPE_TEXT_HTML = "text/html";
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Website website = GinaUtil.getWebsite();

			String path = request.getParameter( GinaServlet.PARAMETER_PATH); //path is set by URL Rewrite 
			boolean doNotRender = request.getParameter("doNotRender") != null;
			
			//create RequestContent
			RequestContext rq = new RequestContext( request, response );

			// default page www.domain.com/ different for basic and advanced sites
			if ( "/".equals( path) ) {				
				if ( website.isBasicWebsite()) {
					path = "/index.html";
				} else {
					// We have to chain
					RequestDispatcher rd = request.getRequestDispatcher("/gina");
					rd.forward( request, response);
				}
			}
			
			// Construct URL
			String urlStr = GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST 
					+ website.getGoogleDriveId()
					+ path;
			
			boolean relativeToRoot = request.getParameter("relative-to-root") != null; // "relativeToRoot" parameter overrides template specification
			if (!relativeToRoot && website.getTemplate() != null && !website.getTemplate().equals("") && website.getTemplate().indexOf("starter-pack:") >= 0) {
				String[] templateBlocks = website.getTemplate().split(":");
				
				if (templateBlocks.length != 2) {
					throw new InternalServerErrorException("Invalid template '" + website.getTemplate() + "'.");
				}
				
				urlStr = GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST
						+ website.getGoogleDriveId()
						+ "/templates/"
						+ templateBlocks[1] 
						+ "/" + path;
			}
			
			//set content type
			String contentType = getContentType(path); 
			response.setContentType(contentType);
			response.setCharacterEncoding("UTF-8");
			
			if (CONTENT_TYPE_TEXT_HTML.equals(contentType) && !doNotRender) {				
				// Read and render 
				String content = GinaUtil.requestUrl(urlStr);
				
				content = ContentRenderer.renderContent(rq, content);
				if(content.contains("<meta name=\"google-site-verification\"") && content.contains("<title>Sign in - Google Accounts</title>")){
				    String language = rq.getLanguage();
				    String error = TranslationService.translate(language, "core", "page-not-found");
				    response.getWriter().println( "<br><div style=\"font-family:sans-serif\"> <strong> "+error+" </strong></div>");
				    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				    return;
				}
				response.getWriter().write(content);
			} else if (CONTENT_TYPE_TEXT_HTML.equals(contentType) && doNotRender) {				
				// Read but don't render anything
				String content = GinaUtil.requestUrl(urlStr);
				response.getOutputStream().print(content);
			} else {
				// Re-stream content
				URL url = new URL(urlStr);
				URLConnection urlConn = url.openConnection();
				
				String connectTimeoutSetting = System.getProperty("net.ginaap.controller.servlets.StaticServlet.connectTimeoutInSecs");
				if (connectTimeoutSetting != null && !connectTimeoutSetting.isEmpty()) {
					try { urlConn.setConnectTimeout(Integer.parseInt(connectTimeoutSetting) * 1000); } catch (Exception e) { }
				}
				
				String readTimeoutSetting = System.getProperty("net.ginaap.controller.servlets.StaticServlet.readTimeoutInSecs");
				if (readTimeoutSetting != null && !readTimeoutSetting.isEmpty()) {
					try { urlConn.setReadTimeout(Integer.parseInt(readTimeoutSetting) * 1000); } catch (Exception e) { }
				}
				
				InputStream is = urlConn.getInputStream();
				byte b[] = new byte[1024];
				
				int data = is.read(b);
				while (data != -1) {
					response.getOutputStream().write(b, 0, data);
					data = is.read(b);
				}
				
				response.getOutputStream().flush();
				is.close();
			}
		} catch (Exception e) {
			String errMsg = e.getMessage() + " for url:" + request.getRequestURI() + " and path:" + request.getParameter("path");
			response.getWriter().println( "<br><div style=\"font-family:sans-serif\">" + errMsg + "</div>");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	private String getContentType(String path) {
		// jpg|jpeg|gif|png|css|js|ico|xml|rss|txt|html
		if (path.endsWith(".png"))
			return "image/png";
		if (path.endsWith(".jpg"))
			return "image/jpeg";
		if (path.endsWith(".jpeg"))
			return "image/jpeg";
		if (path.endsWith(".gif"))
			return "image/gif";
		if (path.endsWith(".css"))
			return "text/css";
		if (path.endsWith(".js"))
			return "application/javascript";
		if (path.endsWith(".ico"))
			return "image/x-icon";
		if (path.endsWith(".xml"))
			return "text/xml";
		if (path.endsWith(".txt"))
			return "text/text";
		if (path.endsWith(".html"))
			return CONTENT_TYPE_TEXT_HTML;

		return null;
	}
}