package net.ginaap.controller.servlets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.CMSResponse;
import net.ginaapp.model.Website;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.CacheService;
import net.ginaapp.services.OfyService;
import net.ginaapp.services.TranslationService;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

public class CMSServlet extends HttpServlet {

	private static final long serialVersionUID = -5484510359252751470L;
	private static final Logger log = Logger.getLogger(CMSServlet.class.getName());
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		CMSResponse resp = new CMSResponse();
		
		RequestContext requestContext = new RequestContext(request, response);
		String errUnauthorized = "You are not authorized to update this content or your login session could have been discontinued after being idle for a period of time.";
		String errGeneric = "An error occurred while processing content updates. Please try again or contact support should the problem persist.";
		String component = request.getParameter("component");
		String pageKey = request.getParameter("pageKey");
		String key = request.getParameter("key");
		String value = request.getParameter("value");
		
		String language = requestContext.getLanguage();
		if (language == null) {
			Objectify ofy = OfyService.ofy();
			Query<Website> q = ofy.query(Website.class);
			Website website = q.get();
			language = website.getDefaultLanguage();
		}
		
		try {
			errUnauthorized = TranslationService.translate(language, "cms", "cms-embedded-styledtext-no-auth");
			errGeneric = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			String username = (user == null) ? null : user.toString(); 
			
			if (username == null || username.isEmpty()) {
				resp.setSuccessful(false);
				resp.setErrorMessage(errUnauthorized);
			} else {
				CMSDataService.put(component, language, key, value);
				// CacheService.delete(pageKey); // TODO: Find out why deleting single records from MemCache does not work properly causing caching issues.
				CacheService.clearAll(); // TODO: Remove after fixing issue above.
				resp.setSuccessful(true);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "An error occurred while processing CMS updates for component '" 
					+ component + "' with key '"
					+ key + "' for language '" 
					+ language + "'.",
					e);
			
			resp.setSuccessful(false);
			resp.setErrorMessage(errGeneric);
		}
		
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(resp.toJSON());
    }

}
