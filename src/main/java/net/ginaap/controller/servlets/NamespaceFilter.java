package net.ginaap.controller.servlets;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import net.ginaapp.model.Website;
import net.ginaapp.services.OfyService;

import com.google.appengine.api.NamespaceManager;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

/**
 * 
 * NamespaceFilter sets the namespace to the domain name.
 * 
 * Example for www.gina-app.net/en/home the namespace is set to www.gina-app.net
 * 
 * @author Lennart.Benoot@mincko.com
 */
public class NamespaceFilter implements javax.servlet.Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		// Make sure set() is only called if the current namespace is not
		// already set.
		if (NamespaceManager.get() == null) {

			String namespace = req.getServerName();

			// If localhost, get domain out of website Object
			if ("localhost".equals(namespace)){
				Objectify ofy = OfyService.ofy();
				Query<Website> q = ofy.query(Website.class);
				Website website = q.get();
				if (website != null ) namespace = website.getDomain();
			}	

			if (req.getParameter("ns") != null)
				namespace = req.getParameter("ns");

			NamespaceManager.set(namespace);
		}
		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}