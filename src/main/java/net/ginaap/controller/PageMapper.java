package net.ginaap.controller;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

import net.ginaapp.GinaUtil;
import net.ginaapp.model.Page;
import net.ginaapp.model.Website;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.services.OfyService;


/**
 * 
 * Maps a request to a Page
 * 
 * @author lennart.benoot@mincko.com
 *
 */
public class PageMapper {
	
	public static Page map( RequestContext requestContext) throws PageMapperException{
		
		String language;
		String  pageName;
		Objectify ofy = OfyService.ofy();
		Website website = GinaUtil.getWebsite();

		if ( "/".equals( requestContext.getOriginalPath())) {
			language = website.getDefaultLanguage();
			pageName = website.getDefaultPage();
		}
		else if ( "articles".equals( requestContext.getPath()[ 2] )) {
			language = requestContext.getPath()[ 1];
			pageName = requestContext.getPath()[ 3];
		}
		else {
			language = requestContext.getPath()[ 1];
			pageName = requestContext.getPath()[ 2];
		}
		requestContext.setLanguage( language);
		requestContext.setPageName( pageName);
		
		Query<Page> q = ofy.query(Page.class).filter("language", language)
				.filter("name", pageName);
		Page page = q.get();
		
		if (page == null)
			throw new PageMapperException( "URL cannot be mapped to an existing page. Verify createFile." );
		
		return page;
		
	}
	
	public static String reverseMap( Page page) {
		
		return "/" + page.getLanguage() + "/" + page.getName();
	} 

}
