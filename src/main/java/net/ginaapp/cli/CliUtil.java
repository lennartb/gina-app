package net.ginaapp.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

import net.ginaapp.GinaUtil;
import net.ginaapp.model.MenuItem;
import net.ginaapp.model.Page;
import net.ginaapp.model.Translation;
import net.ginaapp.model.Website;
import net.ginaapp.services.OfyService;

/**
 * 
 * @author lennart.benoot@mincko.com
 *
 */
public class CliUtil {

	private static final String DEFAULT_PACKAGE = "net.ginaapp.model";

	public static String cleanParameter( String p) {
		
		p=p.trim();
		p=p.replaceAll("\t", "");
		p=p.substring(1, p.length()-1) ;
		
		return p;
	}
	
	public static void createObject(String inputLine) throws Exception {
		// parse into Object and parameters
		Objectify ofy = OfyService.ofy();
		String str[] = inputLine.split(":");
		
		String className = str[0];
		String parameters = str[1];
		
		// Combine the array items from position one onwards as parameters as param args might have ':' within (e.g. starter-pack:five)
		if (str.length > 2) {
			parameters = "";
			for (int i = 1; i < str.length; i++) {
				if (i > 1) parameters += ":";
				parameters += str[i];
			}
		}

		try {
			// Instantiate the object
			String thePackage = DEFAULT_PACKAGE;
			Class theClass = Class.forName(thePackage + "." + className);
			Object obj = theClass.newInstance();
			@SuppressWarnings("unchecked")
			Method method = theClass.getDeclaredMethod("fromString", String.class);
			Object o = method.invoke(obj, parameters);

			ofy.put(obj);
		} catch (Exception e) {
			throw e;
		}
	}

	public static String createFromCreateFile() {

			// check if we can find a create file
			String ret = "";
			String inputLine = "";
			String str = "";

			Website website = GinaUtil.getWebsite();
			
			String googleDriveId = website.getGoogleDriveId();
			if (googleDriveId != null && !"".equals(googleDriveId)) {

				deletesite();
				try {
					//
					URL url = new URL("https://googledrive.com/host/"
							+ googleDriveId + "/createFile");

					BufferedReader in = new BufferedReader(new InputStreamReader(
							url.openStream(), "UTF8"));

					while ((inputLine = in.readLine()) != null) {

						//empty line?
						if (inputLine.length() == 0)
							continue;
						
						//added this (awkward) patch because lines where sometimes starting with a character that caused issues
						char c = inputLine.charAt( 0);
						if ( !Character.isAlphabetic( c) )
							if ( c != '/')
								inputLine = inputLine.substring( 1, inputLine.length());

						// cleanup inputline
						inputLine = inputLine.replaceAll("\t", "");

						if (inputLine.startsWith("//"))
							continue;
						
						if ("".equals(inputLine))
							continue;

						if (inputLine.contains("Page"))
							inputLine = inputLine + "";

						createObject(inputLine);
						ret = ret + "<br>" + inputLine;

					}
					in.close();

					return ret;

				} catch (Exception e) {
					return "An exception occured after the creation of this object: "
							+ inputLine + ": " + e.getMessage();

				}

			}
			else {
				ret = "This website does not seem to be baptized as the Website object is missing or no google drive is available. Please (re)baptize.";
			}
			return ret;
		
	}
	
	

	private static void deletesite() {
		Objectify ofy = OfyService.ofy();

		List<Website> olw = ofy.query(Website.class).list();
		for (Website o : olw)
			ofy.delete(o);

		List<Translation> olt = ofy.query(Translation.class).list();
		for (Translation o : olt)
			ofy.delete(o);

		List<Page> olp = ofy.query(Page.class).list();
		for (Page o : olp)
			ofy.delete(o);

		// delete the menu
		List<MenuItem> olMenu = ofy.query(MenuItem.class).list();
		for (MenuItem oM : olMenu)
			ofy.delete(oM);

	}
	
}
