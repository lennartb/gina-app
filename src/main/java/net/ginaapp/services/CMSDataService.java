package net.ginaapp.services;

import java.util.List;

import net.ginaapp.model.CMSData;
import net.ginaapp.model.StringCache;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

public class CMSDataService {
	
	public static String get(String component, String language, String key){
		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		String cmsDataKey = component + ":" + language + ":" + key;
		String value = (String) cache.get(cmsDataKey);
		
		// if not in cache, try to find in DB
		if (value == null) {
			Objectify ofy = OfyService.ofy();
			Query<CMSData> qpc = ofy.query(CMSData.class)
					.filter("component", component)
					.filter("language", language)
					.filter("key", key);					
			
			CMSData data = qpc.get();
			if (data != null) {
				value = data.getValue();
				cache.put(cmsDataKey, value);
			}
		}
		
		return value;
	}
	
	public static void put (String component, String language, String key, String value) {		
		Objectify ofy = OfyService.ofy();		
		String cmsDataKey = component + ":" + language + ":" + key;
		
		if (component != null && !component.isEmpty()
				&& language != null && !language.isEmpty()
				&& key != null && !key.isEmpty()) {
			
			Iterable<Key<CMSData>> keys = ofy.query(CMSData.class)
					.filter("component", component)
					.filter("language", language)
					.filter("key", key).fetchKeys();
			
			ofy.delete(keys);			
			
			CMSData data = new CMSData();
			data.setComponent(component);
			data.setLanguage(language);
			data.setKey(key);
			data.setValue(value);
			ofy.put(data);
		}

		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		cache.delete(cmsDataKey);
		cache.put(cmsDataKey, value);
	}
	
}
