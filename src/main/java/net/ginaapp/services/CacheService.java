package net.ginaapp.services;

import net.ginaapp.model.StringCache;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

public class CacheService {
	
	public static String get(String key){
		
		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		String value = (String) cache.get( key);
		Objectify ofy = OfyService.ofy();

		// if not in cache, try to find in DB
		if (value == null) {

			Query<StringCache> qpc = ofy.query(StringCache.class).filter("key",key);
			
			StringCache pc = qpc.get();
			if (pc != null) {
				value = pc.getValue();
				cache.put(key, value);
			}
		}
		
		return value;
	}
	
	public static void put(String key, String value){
		
		Objectify ofy = OfyService.ofy();		
		if (key != null && !key.isEmpty()) {
			StringCache sc = new StringCache();
			sc.setKey(key);
			sc.setValue(value);
			ofy.put(sc);
		}

		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		cache.put(key, value);
		
	}
	
	/**
	 * Clear cache entirely
	 */
	public static void clearAll(){
		Objectify ofy = OfyService.ofy();
		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		
		cache.clearAll();
		
		Iterable<Key<StringCache>> allKeys = 
				ofy.query(StringCache.class)
					.fetchKeys();
			
		ofy.delete(allKeys);
		
	}
	
	public static void delete(String key) {
		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		cache.delete(key);
		
		Objectify ofy = OfyService.ofy();
		ofy.delete(StringCache.class, key);
	}


}
