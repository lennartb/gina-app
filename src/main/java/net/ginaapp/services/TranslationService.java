package net.ginaapp.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import net.ginaapp.model.Translation;
import net.ginaapp.model.Website;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

/*
 * Get translation from Google Spreadsheet. 
 * 
 *  Tranlations are retrieved by calling the only public method
 *  translate(String language, String module, String label)
 * 
 * @author lennart.benoot@mincko.com
 */
public class TranslationService {

	private static final String CACHE_KEY_TRANSLATIONS = "translations";

	public static List<String> getLanguages(String resourceId) {
		List<String> languages = new ArrayList<String>();
		
		try {
			HashMap<String, String> translations = getTranslations(resourceId, null);

			if (translations != null) {
				for (String key : translations.keySet()) {
					String[] str = key.split(":");	
					if (str == null || str.length != 3) continue;
					
					String lang = str[0].toUpperCase();
					if (!languages.contains(lang)) languages.add(lang);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Collections.sort(languages);		
		return languages;
	}
	
	public static List<String> getLabelsByModule(String resourceId, String module) {
		List<String> labels = new ArrayList<String>();
		
		try {
			HashMap<String, String> translations = getTranslations(resourceId, null);

			if (translations != null && module != null) {
				module = module.toLowerCase();
				
				for (String key : translations.keySet()) {
					String[] str = key.split(":");	
					if (str == null || str.length != 3) continue;
					
					String mod = str[1].toLowerCase();
					String label = str[2].toLowerCase();
					
					if (mod.equals(module) && !labels.contains(label)) labels.add(label);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Collections.sort(labels);		
		return labels;
	}
	
	public static String translate(String language, String module, String label) {
		return translate(language, module, label, null);
	}
	
	public static String translate(String language, String module, String label, String resourceId) {
		// Use website's default language if the language parameter is null
		if (language == null) {
			Objectify ofy = OfyService.ofy();
			Query<Website> q = ofy.query(Website.class);
			Website website = q.get();
			
			if (website != null) {
				language = website.getDefaultLanguage();
			}
		}		
		
		HashMap<String, String> translations;
		try {
			translations = getTranslations(resourceId, null);
			
			String key = language + ":" + module + ":" + label;
			String translation = translations.get(key);

			return (translation == null) ? key : translation;
		} catch (Exception e) {
			e.printStackTrace();
			return module + ":" + label;
		}
	}

	@SuppressWarnings("unchecked")
	private static HashMap<String, String> getTranslations(String resourceId, String gid) throws Exception {
		HashMap<String, String> translations;

		// Get it from the cache if we already have it
		MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		String cacheKey = (resourceId == null) ? CACHE_KEY_TRANSLATIONS : (CACHE_KEY_TRANSLATIONS + "_" + resourceId);				
		translations = (HashMap<String, String>) cache.get(cacheKey);
		
		if (translations != null) return translations;

		// Not in cache, construct it.
		translations = new HashMap<String, String>();

		if (resourceId == null) {
			// Check if this Website has a translation object of its own
			Objectify ofy = OfyService.ofy();
			Query<Translation> q = ofy.query(Translation.class);
			Translation translationObject = q.get();
			
			if ( translationObject != null) {
				resourceId = translationObject.getGoogleSheetId();
			} else {
				// Mincko default translation spreadsheet
				resourceId = "0Ahm3k6edfFd-dG9uVE91dFVzVFFCVlN0NXFobVVlcnc";
			}
		}
				
		// Use NEW URL format to access translation spreasheet as CSV
		Random rand = new Random();
		String requestUrl = "https://docs.google.com/spreadsheets/d/" + resourceId + "/export?format=csv&gid=0&rand=" + rand.nextInt();
		URL url = new URL(requestUrl);
		URLConnection urlConn = url.openConnection();
		
		String connectTimeoutSetting = System.getProperty("net.ginaapp.services.TranslationService.connectTimeoutInSecs");
		if (connectTimeoutSetting != null && !connectTimeoutSetting.isEmpty()) {
			try { urlConn.setConnectTimeout(Integer.parseInt(connectTimeoutSetting) * 1000); } catch (Exception e) { }
		}
		
		String readTimeoutSetting = System.getProperty("net.ginaapp.services.TranslationService.readTimeoutInSecs");
		if (readTimeoutSetting != null && !readTimeoutSetting.isEmpty()) {
			try { urlConn.setReadTimeout(Integer.parseInt(readTimeoutSetting) * 1000); } catch (Exception e) { }
		}

		InputStream ins = null;
		BufferedReader in = null;
		String inputLine = null;
		
		try {
			ins = urlConn.getInputStream();
			
			// If NEW URL is not returning a CSV then try old URL
			if (!urlConn.getHeaderField("Content-Type").equals("text/csv")) {
				requestUrl = "https://spreadsheets.google.com/pub?key=" + resourceId + "&gid=0&hl=nl&output=csv&rand=" + rand.nextInt();
				url = new URL(requestUrl);
				urlConn = url.openConnection();
				
				if (connectTimeoutSetting != null && !connectTimeoutSetting.isEmpty()) {
					try { urlConn.setConnectTimeout(Integer.parseInt(connectTimeoutSetting) * 1000); } catch (Exception e) { }
				}
				
				if (readTimeoutSetting != null && !readTimeoutSetting.isEmpty()) {
					try { urlConn.setReadTimeout(Integer.parseInt(readTimeoutSetting) * 1000); } catch (Exception e) { }
				}
			}
		} finally {
			if (ins != null) ins.close();
		}
		
		try {
			ins = urlConn.getInputStream();
			in = new BufferedReader(new InputStreamReader(ins, "UTF8"));
			
			// Read header
			inputLine = in.readLine();
			String[] tokens = inputLine.split(",");
			String[] languages = Arrays.copyOfRange(tokens, 2, tokens.length);

			// Get label value pairs
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.equals("")) continue;

				String str[] = inputLine.split(",");
				String module = str[0];
				String token = str[1];

				// for each language put
				for (int i = 0; i < languages.length; i++) {
					String key = languages[i] + ":" + module + ":" + token;
					String translation = (i + 2 < str.length) ? str[i + 2] : key;
					
					translations.put(key, translation);
				}
			}
		} finally {
			if (in != null) in.close();
			if (ins != null) ins.close();
		}

		// Store the translations in cache
		cache.put(cacheKey, translations);
		
		return translations;
	}
}
