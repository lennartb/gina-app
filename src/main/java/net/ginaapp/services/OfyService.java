package net.ginaapp.services;

import net.ginaapp.model.CMSData;
import net.ginaapp.model.MenuItem;
import net.ginaapp.model.Page;
import net.ginaapp.model.Person;
import net.ginaapp.model.StringCache;
import net.ginaapp.model.Translation;
import net.ginaapp.model.Website;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * OfyService takes care of registering data classes and provides easy access to
 * the Objectify instance.
 * 
 * @author lennart.benoot@mincko.com
 * 
 */
public class OfyService {
	static {
		factory().register(Website.class);
		factory().register(Page.class);
		factory().register(MenuItem.class);
		factory().register(StringCache.class);
		factory().register(Person.class);
		factory().register(Translation.class);
		factory().register(CMSData.class);
	}

	public static Objectify ofy() {
		return ObjectifyService.begin();
	}

	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}