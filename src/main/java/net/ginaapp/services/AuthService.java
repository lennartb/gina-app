package net.ginaapp.services;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class AuthService {
	
	public static boolean IsLoggedInUserAdmin() {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		String username = (user == null) ? null : user.toString();
		
		// Delegates the job to authenticate and authorize the user to the admin pages.
		// If the user's email is available, it is assumed that the admin pages has authorized the user.
		return username != null && !username.isEmpty();
	}

}
