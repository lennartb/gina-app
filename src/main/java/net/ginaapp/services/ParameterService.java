package net.ginaapp.services;

import java.util.Enumeration;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import net.ginaap.controller.RequestContext;

public class ParameterService {
	
	private static final String PARAMETER_LANGUAGE = "language";
	private static final String PARAMETER_PAGE = "page";
	private static final String ERROR_UNKNOWN_PARAMETER = "Unknown parameter";

	public static String getParameter( RequestContext context, String parameter) {
		
		if (parameter != null && !parameter.isEmpty()) {
			parameter = parameter.trim();
			
			// Static parameter
			if ( parameter.startsWith( "\""))
				return parameter.substring(1,
						parameter.length() - 1);
			
			// Variable
			if (PARAMETER_PAGE.equals( parameter)) return context.getPageName();
			if (PARAMETER_LANGUAGE.equals( parameter)) return context.getLanguage();
		}
		
		return ERROR_UNKNOWN_PARAMETER;
		
	}
	
	public static <T> T getParameterValue(HttpServletRequest request, String paramName, Class<T> returnType, T defaultValue) {
		Enumeration keys = request.getParameterNames();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();			
			if (key.equals(paramName)) {
				String value = request.getParameter(paramName);				
				try {
					if (returnType == Boolean.class) {
						return (T) Boolean.valueOf(value);
					} else {
						return returnType.cast(value);
					}
				} catch (Exception e) {
					return defaultValue;
				}
			}
		}
		
		return defaultValue;
	}
	
	public static String replacePlaceholders(HttpServletRequest request, String text) {
		if (request != null) {
			Enumeration keys = request.getParameterNames();
			while (keys.hasMoreElements()) {
				String key = keys.nextElement().toString();
				String value = request.getParameter(key);
				
				// Detect input fields by looking for keys with <paramName>-label
				if (key.contains("-label")) {
					String paramName = key.substring(0, key.indexOf("-label"));
					String paramValue = request.getParameter(paramName);
					String placeholder = "\\{%" + paramName + "%\\}";
					text = text.replaceAll(placeholder, paramValue);
				}
			}
		}		
		
		return text;
	}
	
}
