package net.ginaapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Random;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import net.ginaapp.model.Website;
import net.ginaapp.services.OfyService;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

/**
 * 
 * @author Lennart
 *
 */
public class GinaUtil {

	private static final int MAX_RETRY = 5;
	private static final String ERROR_WRONG_GOOGLE_DOC_FORMAT = "Google Doc does not seem to be right format.";
	public static final String HTTPS_GOOGLEDRIVE_COM_HOST = "https://googledrive.com/host/";
	private static final Logger log = Logger
			.getLogger(GinaUtil.class.getName());

	public static Website getWebsite() {
		Objectify ofy = OfyService.ofy();
		Query<Website> q = ofy.query(Website.class);
		Website website = q.get();
		return website;
	}

	public static String requestUrl(String requestUrl) {

		String str = "";
		boolean success = false;
		int retryCount = 1;

		while (retryCount < MAX_RETRY) {
			try {
				//
				URL url = new URL(requestUrl);

				BufferedReader in = new BufferedReader(new InputStreamReader(
						url.openStream(), "UTF8"));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					str = str + inputLine + "\n";

				}
				in.close();

				return str;
			} catch (IOException e) {
				retryCount++;
			}
		}

		return "Error reading file: " + requestUrl;

	}

	public static String readGoogleDoc(String documentId) {
		String content = null;

		String html = "";
		String url = "https://docs.google.com/document/pub?id=" + documentId;
		html = requestUrl(url);

		if (html.contains("<div id=\"contents\">")) {
			// OK, old way google did this works fine in this case
			// get starting point in string
			int indexStart = html.indexOf("<div id=\"contents\">");
			int indexStop = html.indexOf("<div id=\"footer\"");
			html = html.substring(indexStart, indexStop);
			// content = forHTML(html);

			// cut out stylesheet
			indexStart = html.indexOf("<style type=\"text/css\">");
			indexStop = html.indexOf("</style>");
			html = html.substring(0, indexStart)
					+ html.substring(indexStop + 8, html.length());

			// now cut out contents div
			indexStart = html.indexOf("<div id=\"contents\">");
			indexStop = html.lastIndexOf("</div>");
			html = html.substring(indexStart + 19, indexStop);

			// now cut out anchors
			indexStart = html.indexOf("<a ");
			indexStop = html.indexOf("</a>", indexStart);
			if (indexStart != -1) // if -1 there's no anchors in the document.
				html = html.substring(0, indexStart)
						+ html.substring(indexStop + 4, html.length());

			html = escapeHtml(html);
			return html;
		}

		return ERROR_WRONG_GOOGLE_DOC_FORMAT;

	}

	/**
	 * 
	 * 
	 * @param documentId
	 * @return
	 */
	public static String readStyledGoogleDoc(String documentId) {
		String content = null;

		String html = "";
		String url = "https://docs.google.com/document/pub?id=" + documentId;
		html = requestUrl(url);

		if (html.contains("<div id=\"contents\">")) {
			int indexStart = html.indexOf("<div id=\"contents\">");
			int indexStop = html.indexOf("<div id=\"footer\"");
			html = html.substring(indexStart, indexStop);

			// Now cut out css
			indexStart = html.indexOf("<style type=\"");
			indexStop = html.indexOf("</style>");
			String css = html.substring(indexStart, indexStop + 8);

			content = escapeHtml(html);

			// replace class c0,c1,c... with unique identifier
			Random random = new Random();
			int randomInt = random.nextInt();

			int i = 0;
			String targetCss = css;
			targetCss = targetCss.replaceAll("'", "\"");
			while (targetCss.indexOf(".c" + i) != -1) {
				content = content.replaceAll(
						"class=\"c" + i,
						"class=\"c_" + randomInt + "_" + i);

				targetCss = targetCss.replaceAll(
						".c" + i, 
						".c_" + randomInt + "_" + i);
				
				i++;
				
				if (i > 500) {
					content = "Infinite loop while applying stylesheet.";
					continue; // avoid infinite look
				}
			}

			// Maek image url absolute
			content = content.replaceAll("pubimage",
					"https://docs.google.com/document/d/" + documentId
							+ "/pubimage");

			// combine css and content again
			content = targetCss + content;
		}

		return content;
	}

	public static String escapeHtml(String html) {

		html = html.replaceAll("�", "'");

		return html;
	}

}
