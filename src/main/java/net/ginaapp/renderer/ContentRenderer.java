package net.ginaapp.renderer;

import java.util.ArrayList;
import java.util.List;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.TranslationService;

/**
 * 
 * ContentRenderer is responsible for identifying the components in a text and
 * then rendering these components within the text.
 * 
 * @author lennart.benoot@mincko.com
 * 
 */
public class ContentRenderer {

	/* render with requestcontext */
	public static String renderContent(RequestContext reqObj, String content)
			throws RenderException {

		List<String> components = getComponents(content);

		for (String component : components) {
			content = renderComponent(reqObj, content, component);
		}

		return content;
	}

	/**
	 * Example Component:
	 * GoogleDocsText("1P9W77-GIcHPrV_FjcrktmjVoPdsTiqrrhlA_1hb30mM")
	 * 
	 * @param reqObj
	 * @param content
	 * @param component
	 * @return
	 */
	private static String renderComponent(RequestContext requestContext,
			String content, String component) throws RenderException {

		String html = content;
		try {
			// Parse Component
			// Example:
			// GoogleDocsText("","1P9W77-GIcHPrV_FjcrktmjVoPdsTiqrrhlA_1hb30mM")
			String theClass = component.substring(0, component.indexOf('('));
			String parameterStr = component.substring(
					component.indexOf('(') + 1, component.indexOf(')'));
			String parameters[] = parameterStr.split(",");
			String fragment = null;

			//Construt the actual class
			theClass = getFullyQualifiedClass(theClass);
			
			// Create component instance
			IGinaComponent ginaComponent = (IGinaComponent) Class.forName(
					theClass).newInstance();

			// Render component
			ComponentCode cc = ginaComponent.renderComponent(requestContext,
					parameters, fragment);

			// Put Component in html
			if(cc.getHtml().contains("<meta name=\"google-site-verification\"") && cc.getHtml().contains("<title>Sign in - Google Accounts</title>")){
			    String language = requestContext.getLanguage();
			    String componentName = parameters.length >= 2 ? parameters[1]: component;
			    String error = TranslationService.translate(language, "core", "content-rendered-failure");
			    error = error.replace("$component", componentName);
			    html = html.replace("${" + component + "}", error);
			}else{
			html = html.replace("${" + component + "}", cc.getHtml());
			}
			return html;

		} catch (ClassNotFoundException e) {
			html = html.replace("${" + component + "}", component
					+ ": Component class not found.");
			return html;
		} catch (Exception e) {
			throw new RenderException("Unable to render Component: "
					+ component + " due to " + e.getMessage(), e);
		}
	}

	private static List<String> getComponents(String html) {

		List<String> components = new ArrayList<String>();

		String searchFor = "${";

		int len = searchFor.length();
		// int result = 0;

		if (len > 0) {
			int start = html.indexOf(searchFor);
			while (start != -1) {

				int stop = html.indexOf("}", start);
				components.add(html.substring(start + 2, stop));
				start = html.indexOf(searchFor, start + len);
			}
		}

		return components;
	}
	/**
	 * Three cases
	 * 
	 * 1) core component, in the template simply ${Menu}
	 * 2) non-core GinA component, in template ${package.NonCoreComponent}
	 * 2) third-part GinA component, in template ${com.package.Component}
	 * 
	 * @param theClass
	 * @return
	 */
	private static String getFullyQualifiedClass(String theClass) {
		if (!theClass.contains("."))
			return "net.ginaapp.renderer.components.core." + theClass;
		else if (! ( theClass.startsWith("net.")|| theClass.startsWith("com.") || theClass.startsWith("org.")))
			return "net.ginaapp.renderer.components." + theClass;
		else
			return theClass;
	}

}
