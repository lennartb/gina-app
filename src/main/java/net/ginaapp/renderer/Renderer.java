package net.ginaapp.renderer;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.model.Page;
import net.ginaapp.model.Website;
import net.ginaapp.services.TranslationService;


/**
 * 
 * Renderer takes care of the Page rendering process.
 * 
 * @author lennart.benoot@mincko.com
 *
 */
public class Renderer {

	public static String renderPage(RequestContext requestContext, Page page) throws RenderException {
		Website website = GinaUtil.getWebsite();		
		String content;
		
		if (website.getTemplate() != null && !website.getTemplate().equals("") && website.getTemplate().indexOf("starter-pack:") < 0) {
			content = renderTemplate(requestContext, page);
		} else {
			String url = GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST 
					+ website.getGoogleDriveId()
					+ "/" + page.getContentUrl() 
					+ "?doNotRender=true";
			
			boolean relativeToRoot = requestContext.getRequest().getParameter("relative-to-root") != null; // "relativeToRoot" parameter overrides template specification
			if (!relativeToRoot && website.getTemplate() != null && !website.getTemplate().equals("") && website.getTemplate().indexOf("starter-pack:") >= 0) {
				String[] templateBlocks = website.getTemplate().split(":");
				
				if (templateBlocks.length != 2) {
					throw new RenderException("Invalid template '" + website.getTemplate() + "'.");
				}
				
				url = GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST
						+ website.getGoogleDriveId()
						+ "/templates/"
						+ templateBlocks[1] 
						+ "/" + page.getContentUrl() 
						+ "?doNotRender=true";
			}

			content = GinaUtil.requestUrl(url);
		}
		
		//catch & throw exception if there are duplicate html files in shared folder 
		if(content.contains("<meta name=\"google-site-verification\"") && content.contains("<title>Sign in - Google Accounts</title>")){
		    String language = requestContext.getLanguage();
		    String component = page.getContentUrl();
		    String error = TranslationService.translate(language, "core", "page-rendered-failure");
		    error = error.replace("$component", component);
		    throw new RenderException(error);
		}
		int iterations = 0;
		while ( content.contains("${")) {
			content = ContentRenderer.renderContent( requestContext, content);

			iterations++;
			
			if ( iterations > 100) {
				throw new RenderException("Page cannot have more than 100 components. This exception usually occurs when Renderer is in an infinite loop");
			}
		}

		return content;
	}

	public static String renderTemplate( RequestContext requestContext, Page page) {
		Website website = GinaUtil.getWebsite();
		String templateUrl= GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST + website.getGoogleDriveId()
				+ "/" + website.getTemplate(); 

		String contentUrl = GinaUtil.HTTPS_GOOGLEDRIVE_COM_HOST + website.getGoogleDriveId()
				+ "/" + page.getContentUrl();

		String template = GinaUtil.requestUrl( templateUrl);
		String content = GinaUtil.requestUrl( contentUrl);

		content =  template.replace( "${Content(\"\")}", content);

		return content;
	}

}
