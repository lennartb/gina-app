package net.ginaapp.renderer.components;

/**
 * 
 * Generated code by components
 * 
 * @author lennart.benoot@mincko.com
 *
 */
public class ComponentCode {

	private String javascript;
	private String html;
	private String css;
	
	public String getJavascript() {
		return javascript;
	}
	public void setJavascript(String javascript) {
		this.javascript = javascript;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getCss() {
		return css;
	}
	public void setCss(String css) {
		this.css = css;
	}
	
}
