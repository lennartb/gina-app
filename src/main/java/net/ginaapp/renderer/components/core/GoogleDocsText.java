package net.ginaapp.renderer.components.core;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.TranslationService;

/*
 * Get a text from the translations sheet linked to the website. The sheet can contain actual text or a link to a 
 * Google Document.
 * 
 * ${Text("module", "label")}
 * 
 * @deprecated use Text instead.  
 * 
 */
public class GoogleDocsText implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
				
		String googleDocsId = param[1].substring(1,
				param[1].length() - 1);
		String html = GinaUtil.readGoogleDoc(googleDocsId);
		
		ComponentCode cc = new ComponentCode();
		cc.setHtml(html);
		
		return cc;
	}

}
