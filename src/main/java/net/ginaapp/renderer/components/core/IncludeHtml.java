package net.ginaapp.renderer.components.core;

import com.google.appengine.api.NamespaceManager;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.model.Website;
import net.ginaapp.renderer.ContentRenderer;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.OfyService;

/*
 * 
 * 
 * 
 */
public class IncludeHtml implements IGinaComponent {

	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {

		Objectify ofy = OfyService.ofy();
		Query<Website> q = ofy.query(Website.class);
		Website website = q.get();

		String path = param[1].substring(1, param[1].length() - 1);

		String url = "http://" + ( requestContext.isLocalhost() ? "localhost:8888" : website.getDomain()) + "/" + path + "?doNotRender=true";
		String html = GinaUtil.requestUrl(url);

		// html could contain Gina Components, check and if so, render.
		//if ( html.contains( "${"))
		//	html = ContentRenderer.renderContent(requestContext, html);

		ComponentCode cc = new ComponentCode();
		cc.setHtml(html);

		return cc;
	}

}
