package net.ginaapp.renderer.components.core;

import java.util.Random;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.CacheService;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

public class Image implements IGinaComponent {
	
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	// ${Image("key","altTranslationKey","defaultImagePath","width","height","additionalAttributes")}
	// Renders (if no authorization to edit): <img src="{pathToBlobStoreServlet|defaultImagePath}" alt="{Translated alternate text based on altTranslationKey}" width="{width}" height="{height}" {additionalAttributes} />
	// Renders (if has authorization to edit): <img data-cms-image="{key}" src="{pathToBlobStoreServlet|defaultImagePath}" alt="{Translated alternate text based on altTranslationKey}" width="{width}" height="{height}" {additionalAttributes} />

	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {
				
		ComponentCode cc = new ComponentCode();
		String component = "image";
		String pageKey = requestContext.getLanguage() + requestContext.getPageName();
		String language = requestContext.getLanguage();
		String key = ParameterService.getParameter(requestContext, param[0]);
		String altKey = ParameterService.getParameter(requestContext, param[1]);
		String alt = (altKey == null || altKey.isEmpty()) ? "" : TranslationService.translate(language, "texts", altKey);
		String defaultImagePath = ParameterService.getParameter(requestContext, param[2]);
		String width = ParameterService.getParameter(requestContext, param[3]);
		String height = ParameterService.getParameter(requestContext, param[4]);
		String additionalAttrs = ParameterService.getParameter(requestContext, param[5]);
		String targetWidth = width;
		String targetHeight = height;
		if(param.length == 8){
    			targetWidth = param[6] != null ? ParameterService.getParameter(requestContext, param[6]): width;
    			targetHeight = param[7] != null ? ParameterService.getParameter(requestContext, param[7]): height;
		}
		Random rnd = new Random();
		int max = 99999;
		int min = 10000;
		int rndNbr = rnd.nextInt((max - min) + 1) + min;
		
		String src = CMSDataService.get(component, language, key);
		if (src == null) src = defaultImagePath;
		else src = "/serve?blob-key=" + src;
		
		String attr = "";
		if (AuthService.IsLoggedInUserAdmin()) attr = " data-cms-image=\"" + key + "\" data-cms-page-key=\"" + pageKey + "\"";

		String html = "<div id=\"cms-image-" + rndNbr + "\" class=\"cms-image-container\"" + attr + ">" 
				+ "  <img"
				+ "    src=\"" + src + "\""
				+ "    alt=\"" + alt + "\""
				+ "    width=\"" + width + "\""
				+ "    height=\"" + height + "\""
				+ ((additionalAttrs != null && !additionalAttrs.isEmpty()) ? (" " + additionalAttrs) : "")				
				+ " />";
		
		if (AuthService.IsLoggedInUserAdmin()) {
			String title = TranslationService.translate(language, "cms", "cms-image-editor-title");
			String upload = TranslationService.translate(language, "cms", "cms-image-upload");
			String save = TranslationService.translate(language, "cms", "cms-image-save");
			String saving = TranslationService.translate(language, "cms", "cms-image-saving");
			String cancel = TranslationService.translate(language, "cms", "cms-image-cancel");
			String crop = TranslationService.translate(language, "cms", "cms-image-crop");
			String dismissError = TranslationService.translate(language, "cms", "cms-image-dismiss-error");
			String genericError = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			String cropInstructions = TranslationService.translate(language, "cms", "cms-image-crop-instructions");
			
			html += "<span class=\"fa-stack fa-lg cms-inline-edit-button\" data-parent=\"image-" + rndNbr + "\">" +
					"  <i class=\"fa fa-circle fa-stack-2x text-danger\"></i>" +
					"  <i class=\"fa fa-photo fa-stack-1x fa-inverse\"></i>" +
					"</span>";
			
			html += "<div class=\"modal fade cms-image-editor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\" data-parent=\"cms-image-" + String.valueOf(rndNbr) + "\" data-cms-image-src=\"" + src + "\" data-cms-image-width=\"" + width + "\" data-cms-image-height=\"" + height + "\">"
					+ "  <div class=\"modal-dialog\">"
					+ "    <div class=\"modal-content\">"
					+ "      <div class=\"modal-header\">"
					+ "        <h4 class=\"modal-title\" id=\"modalLabel\">" + title + "</h4>"
					+ "      </div>"
					+ "      <div class=\"modal-body\">"
					+ "        <div class=\"modal-body-viewer\" >"
					+ "          <div class=\"viewing-container\"></div>"
					+ "        </div>"
					+ "        <div class=\"modal-body-editor\" >"
					+ "          <p class=\"crop-instructions\">" + cropInstructions + "</p>"
					+ "          <div class=\"cropping-container\"></div>"
					+ "        </div>"
					+ "        <div class=\"modal-body-saving\">"
					+ "          <br /><i class=\"fa fa-spin fa-circle-o-notch fa-4x\"></i><br /><br /><div>" + saving + "</div><br /><br />"
					+ "        </div>"
					+ "        <div class=\"modal-body-error\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\"></div>"
					+ "        </div>"
					+ "        <div class=\"modal-body-error-generic\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\">" + genericError + "</div>"
					+ "        </div>"
					+ "      </div>"
					+ "      <div class=\"modal-footer\">"
					+ "        <div class=\"modal-footer-viewer\">"
					+ "              <button type=\"button\" class=\"btn btn-link btn-cancel\" data-dismiss=\"modal\">" + cancel + "</button>"
					+ "              <button type=\"button\" class=\"btn btn-default btn-upload\">" + upload + "</button>"
					+ "              <input type=\"file\" class=\"select-file\" accept=\"image/*\" />"
					+ " 	   </div>"					
					+ "        <div class=\"modal-footer-editor\">"
					+ "              <input type=\"hidden\" name=\"key\" value=\"" + key + "\" />"
					+ "              <input type=\"hidden\" name=\"component\" value=\"image\" />"
					+ "              <input type=\"hidden\" name=\"pageKey\" value=\"" + pageKey + "\" />"
					+ "              <input type=\"hidden\" name=\"language\" value=\"" + language + "\" />"
					+ "              <button type=\"button\" class=\"btn btn-link btn-cancel\" data-dismiss=\"modal\">" + cancel + "</button>"
					+ "              <button type=\"button\" class=\"btn btn-default btn-save\">" + save + "</button>"
					+ "            </form>"
					+ "        </div>"
					+ "        <div class=\"modal-footer-error\">"
					+ "          <button type=\"button\" class=\"btn btn-primary btn-dismiss-error\" data-dismiss=\"modal\">" + dismissError + "</button>"
					+ "        </div>"
					+ "      </div>"
					+ "    </div>"
					+ "  </div>"
					+ "</div>";
			
//			html += "<div class=\"modal fade cms-image-editor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\" data-parent=\"cms-image-" + String.valueOf(rndNbr) + "\" data-cms-image-src=\"" + src + "\">"
//					+ "  <div class=\"modal-dialog\">"
//					+ "    <div class=\"modal-content\">"
//					+ "      <div class=\"modal-header\">"
//					+ "        <h4 class=\"modal-title\" id=\"modalLabel\">" + title + "</h4>"
//					+ "      </div>"
//					+ "      <div class=\"modal-body\">"
//					+ "        <div class=\"modal-body-editor\" >"
//					+"	 	<div id=\"parent\" style=\"position:relative;\"> "	
//					+ "          <img src=\"" + src + "\" width=\"" + width + "\" height=\"" + height + "\" style=\"box-sizing:content-box; -moz-box-sizing:content-box; -webkit-box-sizing:content-box;\"/>"
//					//+ "          <img class=\"resize-image\" src=\"" + src + "\" />"
//					+ "        </div>"
//					+ "        </div>"
//					+ "        <div class=\"modal-body-cropping\">"
//					+ "        </div>"
//					+ "        <div class=\"modal-body-saving\">"
//					+ "          <br /><i class=\"fa fa-spin fa-circle-o-notch fa-4x\"></i><br /><br /><div>" + saving + "</div><br /><br />"
//					+ "        </div>"
//					+ "        <div class=\"modal-body-error\">"
//					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\"></div>"
//					+ "        </div>"
//					+ "        <div class=\"modal-body-error-generic\">"
//					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\">" + genericError + "</div>"
//					+ "        </div>"
//					+ "      </div>"
//					+ "      <div class=\"modal-footer\">"
//					+ "        <div class=\"modal-footer-editor\">"
//					+ "            <form method=\"POST\" action=\"" + blobstoreService.createUploadUrl("/upload") + "\" enctype=\"multipart/form-data\">"
//					+ "              <button type=\"button\" class=\"btn btn-default btn-upload\">" + upload + "</button>"
//					+ "              <input type=\"file\" name=\"upload\" class=\"file-upload\" accept=\"images/*\" />"
//					+ "              <input type=\"hidden\" name=\"key\" value=\"" + key + "\" />"
//					+ "              <input type=\"hidden\" name=\"component\" value=\"image\" />"
//					+ "              <input type=\"hidden\" name=\"pageKey\" value=\"" + pageKey + "\" />"
//					+ "              <input type=\"hidden\" name=\"language\" value=\"" + language + "\" />"
//					+ "              <button type=\"button\" class=\"btn btn-link btn-cancel\" data-dismiss=\"modal\">" + cancel + "</button>"
//					+ "              <button type=\"button\" class=\"btn btn-default btn-save\">" + save + "</button>"
//					+ "              <button type=\"button\" class=\"btn btn-default btn-crop\">" + crop + "</button>"
//					+ "		<input type=\"hidden\" name=\"x1\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"y1\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"x2\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"y2\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"targetHeight\" value=\"" + targetHeight + "\" />"
//					+ "		<input type=\"hidden\" name=\"targetWidth\" value=\"" + targetWidth + "\" />"
//					+ "		<input type=\"hidden\" name=\"oriHeight\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"oriWidth\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"status\" value=\"\" />"
//					+ "		<input type=\"hidden\" name=\"cropBlobKey\" value=\"\" />"
//					+ "            </form>"
//					+ "        </div>"
//					+ "        <div class=\"modal-footer-error\">"
//					+ "          <button type=\"button\" class=\"btn btn-primary btn-dismiss-error\" data-dismiss=\"modal\">" + dismissError + "</button>"
//					+ "        </div>"
//					+ "      </div>"
//					+ "    </div>"
//					+ "  </div>"
//					+ "</div>";
		}
		
		html += "</div>";

		cc.setHtml(html);

		return cc;
	}

}
