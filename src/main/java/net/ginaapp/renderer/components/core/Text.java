package net.ginaapp.renderer.components.core;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

/*
 * 
 * 
 * ${Text("","module","label")}
 * 
 */
public class Text implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		String module = ParameterService.getParameter( requestContext, param[ 1]);
		String label =  ParameterService.getParameter( requestContext, param[ 2]);
		
		
		// using translate service to get Text
		String text = TranslationService.translate( requestContext.getLanguage(), module, label);

		//Check if text is a google docs ID such as 1HkIwTIcyeMXuMWBstswjw11i8lrLkfjH86UJCfZ3CVI
		if ( text.length() == 44)
			if (  !text.contains( " "))
				text = GinaUtil.readGoogleDoc( text);
				
		ComponentCode cc = new ComponentCode();
		cc.setHtml( text);
		
		return cc;
	}

}
