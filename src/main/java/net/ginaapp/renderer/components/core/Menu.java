package net.ginaapp.renderer.components.core;

import java.util.List;

import com.googlecode.objectify.Objectify;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.MenuItem;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.OfyService;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

/*
 * 
 * 
 * ${GoogleDocsText("1P9W77-GIcHPrV_FjcrktmjVoPdsTiqrrhlA_1hb30mM")}
 * 
 */
public class Menu implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		ComponentCode cc = new ComponentCode();
		Objectify ofy = OfyService.ofy();	

		String html = " <ul class=\"nav navbar-nav\">";
		String language = requestContext.getLanguage();
		String save = TranslationService.translate(language, "cms", "cms-color-picker-save");
		String cancel = TranslationService.translate(language, "cms", "cms-color-picker-cancel");
		String showLanguageSelection = null;
		String defaultLanguageChangedPath = null;
		
		if (param.length >= 2) showLanguageSelection = ParameterService.getParameter(requestContext, param[1]);
		if (param.length >= 3) defaultLanguageChangedPath = ParameterService.getParameter(requestContext, param[2]);
		
		List<MenuItem> menuItemList = ofy.query(MenuItem.class).order ("menuorder").list();
		for (MenuItem menuItem : menuItemList) {
			
			//create href
			String href = "/" + requestContext.getLanguage() + "/" + menuItem.getMenuId();
			String menuLabel = TranslationService.translate( requestContext.getLanguage(), "menu", menuItem.getMenuId());
			
			//active or not?
			String active = menuItem.getMenuId().equals( requestContext.getPageName()) ? "class=\"active\"" : "";
			
			html = html + "<li "+active+">" 
					+  "<a href=\""+href+"\">"+menuLabel+"</a>"
					+ "</li>";	
		}
		
		// Inject color picker button and dialog into menu if the user is authenticated and has authorization to edit styles
		if (AuthService.IsLoggedInUserAdmin()) {
			html += "<li>"
					+ "  <a class=\"cms-color-picker-trigger\" href=\"#\"><span class=\"glyphicon glyphicon-adjust\"></span></a>"
					+ "  <div class=\"cms-color-picker well\">"
					+ "    <form class=\"cms-color-picker-items\">"
					+ "    </form>"
					+ "    <div class=\"color-picker-buttons\">" 
					+ "      <button class=\"btn btn-default save\">" + save + "</button>" 
					+ "      <button class=\"btn btn-link cancel\">" + cancel + "</button>" 
					+ "    </div>"
					+ "  </div>"
					+ "</li>";
		}
		
		if (showLanguageSelection != null && showLanguageSelection.toLowerCase().equals("true")) {
			List<String> languages = TranslationService.getLanguages(null);
			if (languages != null) {
				int langCount = 0;
				String langSelector = "";
				for (String lang : languages) {
					if (!lang.toUpperCase().equals(language.toUpperCase())) {
						if (langCount > 0) langSelector += "|";					
						langSelector += "<a href=\"/" + lang.toLowerCase() + "/" 
								+ ((defaultLanguageChangedPath == null || defaultLanguageChangedPath.isEmpty()) ? "" : defaultLanguageChangedPath) 
								+ "\">" + lang.toUpperCase() + "</a>";
						
						langCount++;
					}
				}
				
				if (!langSelector.isEmpty()) {
					html += "<li>" + langSelector + "</li>";
				}
			}
		}
		
		html += "</ul>";		
		cc.setHtml(html);
		
		return cc;
	}

}
