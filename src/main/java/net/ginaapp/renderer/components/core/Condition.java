package net.ginaapp.renderer.components.core;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;

/*
 * Very basic version of a condition component. 
 * If parameter 1 equals to current path, parameter 2 is shown.
 * 
 * @author: Lennart Benoot
 */
public class Condition implements IGinaComponent {

	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {

		String html = "";

		String condition = param[ 1].contains( "\"") ? param[1].substring( 1, param[1].length()-1) : param[1];
		String originalPath = requestContext.getOriginalPath();
		
		if ( condition.equals( originalPath)) 
			html = param[2].substring( 1, param[2].length() - 1);

		ComponentCode cc = new ComponentCode();
		cc.setHtml( html);

		return cc;
	}

}
