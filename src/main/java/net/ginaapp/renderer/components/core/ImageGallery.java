package net.ginaapp.renderer.components.core;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.ginaap.controller.RequestContext;
import net.ginaap.controller.servlets.CMSImageGalleryUploadServlet;
import net.ginaapp.model.CMSImageGallery;
import net.ginaapp.model.CMSImageGalleryItem;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class ImageGallery implements IGinaComponent {
	
	private static BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	
	private static final Logger log = Logger.getLogger(ImageGallery.class.getName());
	
	private static final String COMPONENT_NAME = "imageGallery";
	
	// ${ImageGallery("key","additionalAttributes")}
	// Renders (if no authorization to edit): <img src="{pathToBlobStoreServlet|defaultImagePath}" alt="{Translated alternate text based on altTranslationKey}" width="{width}" height="{height}" {additionalAttributes} />
	// Renders (if has authorization to edit): <img data-cms-image="{key}" src="{pathToBlobStoreServlet|defaultImagePath}" alt="{Translated alternate text based on altTranslationKey}" width="{width}" height="{height}" {additionalAttributes} />

	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {
				
		ComponentCode cc = new ComponentCode();
		String pageKey = requestContext.getLanguage() + requestContext.getPageName();
		String language = requestContext.getLanguage();
		String key = ParameterService.getParameter(requestContext, param[0]);
		String additionalAttrs = ParameterService.getParameter(requestContext, param[1]);
		cc.setHtml(ImageGallery.getComponentHTML(language, key, pageKey, additionalAttrs));
		
		return cc;
		
	}
	
	public static String getComponentHTML(String language, String key, String pageKey, String additionalAttrs) {
		
		CMSImageGallery gallery = null;
		String json = CMSDataService.get(ImageGallery.COMPONENT_NAME, language, key);
		String galleryStr = "";
		try {
			if (json != null && !json.isEmpty()) gallery = CMSImageGallery.fromJson(json);			
			if (gallery != null && gallery.getItems().size() > 0) {
				for (CMSImageGalleryItem item : gallery.getItems()) {
					String title = item.getTitle();
					String description = item.getDescription();
					Boolean hasTitle = (title != null && !title.isEmpty());
					Boolean hasDescription = (description != null && !description.isEmpty());
					
					galleryStr += "<div class=\"cms-image-item\">"
							+ "<a href=\"\" data-lightbox=\"" + key + "\""
							+ (hasTitle ? " data-title=\"" + title + "\"" : "") + ">"
							+ "<img width=\"100%\" class=\"img-responsive\" src=\"/serve?blob-key=" + item.getBlobKey() + "\" /></a>"
							+ ((hasTitle || hasDescription) ? "<div class=\"caption\">" : "")
							+ (hasTitle ? ("<h4 class=\"title\">" + title + "</h4>") : "")
							+ (hasDescription ? ("<div class=\"description\">" + description + "</div>") : "")
							+ ((hasTitle || hasDescription) ? "</div>" : "")
							+ "</div>";
				}
			}
		} catch (Exception ex) {
			log.log(Level.SEVERE, 
					"Could not deserialize JSON to CMSImageGallery. JSON: " + json,
					ex);
		}
				
		String attr = "";
		if (AuthService.IsLoggedInUserAdmin()) attr = " data-cms-image-gallery=\"" + key + "\" data-cms-page-key=\"" + pageKey + "\"";

		Random rnd = new Random();
		int max = 99999;
		int min = 10000;
		int rndNbr = rnd.nextInt((max - min) + 1) + min;
		String html = "<div id=\"cms-image-gallery-" + rndNbr + "\" class=\"cms-image-gallery-container\"" + attr 
				+ ((additionalAttrs != null && !additionalAttrs.isEmpty()) ? (" " + additionalAttrs) : "") + ">"
				+ "<div class=\"cms-image-gallery\">" + galleryStr + "</div>";
		
		if (AuthService.IsLoggedInUserAdmin()) {
			String title = TranslationService.translate(language, "cms", "cms-image-gallery-editor-title");
			String addImage = TranslationService.translate(language, "cms", "cms-image-gallery-add");
			String reorderInstructions = TranslationService.translate(language, "cms", "cms-image-gallery-reorder-instructions");
			String save = TranslationService.translate(language, "cms", "cms-image-gallery-save");
			String saving = TranslationService.translate(language, "cms", "cms-image-gallery-saving");
			String cancel = TranslationService.translate(language, "cms", "cms-image-gallery-cancel");
			String dismissError = TranslationService.translate(language, "cms", "cms-image-gallery-dismiss-error");
			String genericError = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			String titleLabel = TranslationService.translate(language, "cms", "cms-image-gallery-title-label");
			String descriptionLabel = TranslationService.translate(language, "cms", "cms-image-gallery-description-label");
			String encodedAdditionalAttrs = "";
			
			try {
				encodedAdditionalAttrs = URLEncoder.encode(((additionalAttrs != null && !additionalAttrs.isEmpty()) ? additionalAttrs : ""), "UTF-8");
			} catch (UnsupportedEncodingException e) { }
			
			html += "<span class=\"fa-stack fa-lg cms-inline-edit-button\" data-parent=\"image-gallery-" + rndNbr + "\">" 
					+ "  <i class=\"fa fa-circle fa-stack-2x text-danger\"></i>" 
					+ "  <i class=\"fa fa-th-large fa-stack-1x fa-inverse\"></i>" 
					+ "</span>";
			
			html += "<div class=\"modal fade cms-image-gallery-editor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\" data-parent=\"cms-image-gallery-" + String.valueOf(rndNbr) + "\">"
					+ "  <div class=\"modal-dialog\">"
					+ "    <div class=\"modal-content\">"
					+ "      <div class=\"modal-header\">"
					+ "        <h4 class=\"modal-title\" id=\"modalLabel\">" + title + "</h4>"
					+ "      </div>"
					+ "      <div class=\"modal-body\">"
					+ "        <div class=\"modal-body-editor\">"
					+ "            <input type=\"hidden\" name=\"titleLabel\" value=\"" + titleLabel + "\" />"
					+ "            <input type=\"hidden\" name=\"descriptionLabel\" value=\"" + descriptionLabel + "\" />"
					+ "          <form method=\"POST\" action=\"" + blobstoreService.createUploadUrl("/gallery-upload") + "\" enctype=\"multipart/form-data\">"
					+ "            <input type=\"hidden\" name=\"key\" value=\"" + key + "\" />"
					+ "            <input type=\"hidden\" name=\"component\" value=\"" + ImageGallery.COMPONENT_NAME + "\" />"
					+ "            <input type=\"hidden\" name=\"pageKey\" value=\"" + pageKey + "\" />"
					+ "            <input type=\"hidden\" name=\"language\" value=\"" + language + "\" />"
					+ "            <input type=\"hidden\" name=\"additionalAttributes\" value=\"" + encodedAdditionalAttrs + "\" />"
					+ "            <ul class=\"cms-image-gallery-item-list\"></ul>"
					+ "		     </form>"
					+ "        </div>"
					+ "        <div class=\"modal-body-saving\">"
					+ "          <br /><i class=\"fa fa-spin fa-circle-o-notch fa-4x\"></i><br /><br /><div>" + saving + "</div><br /><br />"
					+ "        </div>"
					+ "        <div class=\"modal-body-error\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\"></div>"
					+ "        </div>"
					+ "        <div class=\"modal-body-error-generic\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\">" + genericError + "</div>"
					+ "        </div>"
					+ "      </div>"
					+ "      <div class=\"modal-footer\">"
					+ "        <div class=\"modal-footer-editor\">"
					+ "          <div>" + reorderInstructions + "</div>"
					+ "          <div>"
					+ "			   <button type=\"button\" class=\"btn btn-default btn-add-image\">" + addImage + "</button>"
					+ "            <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">" + cancel + "</button>"
					+ "            <button type=\"button\" class=\"btn btn-default btn-save\">" + save + "</button>"
					+ "          </div>"
					+ "        </div>"
					+ "        <div class=\"modal-footer-error\">"
					+ "          <button type=\"button\" class=\"btn btn-primary btn-dismiss-error\" data-dismiss=\"modal\">" + dismissError + "</button>"
					+ "        </div>"
					+ "      </div>"
					+ "    </div>"
					+ "  </div>"
					+ "</div>";
		}
		
		html += "</div>";
		
		return html;
	}

}