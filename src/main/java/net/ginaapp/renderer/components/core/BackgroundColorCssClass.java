package net.ginaapp.renderer.components.core;

import java.util.Random;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

public class BackgroundColorCssClass implements IGinaComponent {

	// ${BackgroundColorCssClass("cssClassName","titleTranslationKey","defaultColor","class='additional-attributes' data-custom='values'")}
	// Renders (if no authorization to edit): <style>.{cssClassName} { background-color: {dataStoreColor|defaultColor}; }</style>
	// Renders (if has authorization to edit): <style data-cms-bgcolor-css-class="{cssClassName}" data-cms-bgcolor-css-title="{Translated text based on titleTranslationKey}" data-cms-bgcolor-css-initial="{Initial color value}">.{cssClassName} { background-color: {dataStoreColor|defaultColor}; }</style>	
	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {

		ComponentCode cc = new ComponentCode();
		String component = "bgcolor-css";
		String pageKey = requestContext.getLanguage() + requestContext.getPageName();
		String language = requestContext.getLanguage();
		String cssClassName = ParameterService.getParameter(requestContext, param[0]);
		String titleTranslationKey = ParameterService.getParameter(requestContext, param[1]);
		String defaultColor = ParameterService.getParameter(requestContext, param[2]);
		String additionalAttrs = ParameterService.getParameter(requestContext, param[3]);

		Random rnd = new Random();
		int max = 99999;
		int min = 10000;
		int rndNbr = rnd.nextInt((max - min) + 1) + min;

		String content = CMSDataService.get(component, language, cssClassName);
		if (content == null) content = defaultColor;

		String attr = "";
		if (AuthService.IsLoggedInUserAdmin()) {
			attr += " id=\"bgcolor-css-" + rndNbr + "\"";
			attr += " data-cms-bgcolor-css-class=\"" + cssClassName + "\"";
			attr += " data-cms-page-key=\"" + pageKey + "\"";
			attr += " data-cms-bgcolor-css-title=\"" + TranslationService.translate(language, "texts", titleTranslationKey) + "\"";
			attr += " data-cms-color-css-colorpicker-choose-text=\"" + TranslationService.translate(language, "cms", "cms-color-picker-save") + "\"";
			attr += " data-cms-color-css-colorpicker-cancel-text=\"" + TranslationService.translate(language, "cms", "cms-color-picker-cancel") + "\"";
			attr += " data-cms-bgcolor-css-initial=\"" + content + "\"";
		}

		String html = "<style" + attr 
				+ ((additionalAttrs != null && !additionalAttrs.isEmpty()) ? (" " + additionalAttrs) : "")				
				+ ">." + cssClassName + " { background-color: " + content + " !important; }</style>";

		cc.setHtml(html);

		return cc;

	}

}
