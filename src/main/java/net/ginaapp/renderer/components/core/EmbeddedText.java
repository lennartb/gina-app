package net.ginaapp.renderer.components.core;

import java.util.Random;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.RenderException;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.AuthService;
import net.ginaapp.services.CMSDataService;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

public class EmbeddedText implements IGinaComponent {

	// ${EmbeddedText("key","Default Text","{editor-dialog-title-key}","class='additional-attributes' data-custom='values'")}
	// Renders (if no authorization to edit): <div {additional-attributes-as-specified}>{content_from_data_store_or_default_text}</div>
	// Renders (if has authorization to edit): <div data-cms-embedded-text="{key}" {additional-attributes-as-specified}>{content_from_data_store_or_default_text}</div>	
	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException {

		ComponentCode cc = new ComponentCode();
		String component = "embedded-text";
		String pageKey = requestContext.getLanguage() + requestContext.getPageName();
		String language = requestContext.getLanguage();
		String key = ParameterService.getParameter(requestContext, param[0]);
		String defaultText = ParameterService.getParameter(requestContext, param[1]);
		String editorTitleKey = ParameterService.getParameter(requestContext, param[2]);
		String additionalAttrs = ParameterService.getParameter(requestContext, param[3]);
		
		Random rnd = new Random();
		int max = 99999;
		int min = 10000;
		int rndNbr = rnd.nextInt((max - min) + 1) + min;

		String content = CMSDataService.get(component, language, key);
		if (content == null) content = defaultText;

		String injectEditor = "";
		if (AuthService.IsLoggedInUserAdmin()) {
			// Add in-line edit button if the user is allowed to edit the content
			injectEditor += "<span class=\"fa-stack fa-lg cms-inline-edit-button\">" +
					"  <i class=\"fa fa-circle fa-stack-2x text-danger\"></i>" +
					"  <i class=\"fa fa-pencil fa-stack-1x fa-inverse\"></i>" +
					"</span>";

			// Add pop up editor if the user is allowed to edit the content
			String title = TranslationService.translate(language, "texts", editorTitleKey);
			String save = TranslationService.translate(language, "cms", "cms-embedded-text-save");
			String saving = TranslationService.translate(language, "cms", "cms-embedded-text-saving");
			String cancel = TranslationService.translate(language, "cms", "cms-embedded-text-cancel");
			String dismissError = TranslationService.translate(language, "cms", "cms-embedded-text-dismiss-error");
			String genericError = TranslationService.translate(language, "cms", "cms-common-err-processing-update");
			
			injectEditor += "<div class=\"modal fade cms-embedded-text-editor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\" data-parent=\"cms-embedded-text-" + String.valueOf(rndNbr) + "\">"
					+ "  <div class=\"modal-dialog\">"
					+ "    <div class=\"modal-content\">"
					+ "      <div class=\"modal-header\">"
					+ "        <h4 class=\"modal-title\" id=\"modalLabel\">" + title + "</h4>"
					+ "      </div>"
					+ "      <div class=\"modal-body\">"
					+ "        <div class=\"modal-body-editor\">"
					+ "          <textarea id=\"cms-embedded-text-editor-" + String.valueOf(rndNbr) + "\" autofocus></textarea>"
					+ "        </div>"
					+ "        <div class=\"modal-body-saving\">"
					+ "          <br /><i class=\"fa fa-spin fa-circle-o-notch fa-4x\"></i><br /><br /><div>" + saving + "</div><br /><br />"
					+ "        </div>"
					+ "        <div class=\"modal-body-error\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\"></div>"
					+ "        </div>"
					+ "        <div class=\"modal-body-error-generic\">"
					+ "          <i class=\"fa fa-exclamation-triangle fa-4x text-danger\"></i><br /><br /><div class=\"error-message\">" + genericError + "</div>"
					+ "        </div>"
					+ "      </div>"
					+ "      <div class=\"modal-footer\">"
					+ "        <div class=\"modal-footer-editor\">"
					+ "          <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">" + cancel + "</button>"
					+ "          <button type=\"button\" class=\"btn btn-default btn-save\">" + save + "</button>"
					+ "        </div>"
					+ "        <div class=\"modal-footer-error\">"
					+ "          <button type=\"button\" class=\"btn btn-default btn-dismiss-error\" data-dismiss=\"modal\">" + dismissError + "</button>"
					+ "        </div>"
					+ "      </div>"
					+ "    </div>"
					+ "  </div>"
					+ "</div>";
		}

		String html = "<div" +
				(AuthService.IsLoggedInUserAdmin() ? " id=\"cms-embedded-text-" + String.valueOf(rndNbr) + "\" data-cms-embedded-text=\"" + key + "\" data-cms-page-key=\"" + pageKey + "\"" : "") +
				((additionalAttrs != null && !additionalAttrs.isEmpty()) ? (" " + additionalAttrs.trim()) : "") +
				"><div class=\"content\">" + content + "</div>" + injectEditor + "</div>";

		cc.setHtml(html);

		return cc;

	}
}