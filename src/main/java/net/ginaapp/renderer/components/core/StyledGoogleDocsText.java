package net.ginaapp.renderer.components.core;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;

/*
 * 
 * 
 * ${GoogleDocsText("1P9W77-GIcHPrV_FjcrktmjVoPdsTiqrrhlA_1hb30mM")}
 * 
 * @deprecated use StyledText instead.  
 * 
 */
public class StyledGoogleDocsText implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		String googleDocsId = param[1].substring(1,
				param[1].length() - 1);
		String html = GinaUtil.readStyledGoogleDoc( googleDocsId);
		
		ComponentCode cc = new ComponentCode();
		cc.setHtml(html);
		
		return cc;
	}
	


}
