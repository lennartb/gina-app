package net.ginaapp.renderer.components.form;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

/*
 * 
 * 
 * ${registereddownload.RegisteredDownload("1P9W77-GIcHPrV_FjcrktmjVoPdsTiqrrhlA_1hb30mM")}
 * 
 */
public class Form implements IGinaComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.ginaapp.renderer.components.IGinaComponent#renderComponent(net.ginaap.controller.RequestContext, java.lang.String[], java.lang.String)
	 * Parameter 3 if provided (String): Forces language choice. 
	 * Parameter 4 if provided (String): Label translation id for send contact request button.
	 * Parameter 2 values (String):
	 * text:<name>:<label-translation-id>
	 * textarea:<name>:<label-translation-id>
	 * text:firstname:<label-translation-id> - Visitor's first name. Mandatory.
	 * text:lastname:<label-translation-id> - Visitor's last name. Mandatory.
	 * text:email:<label-translation-id> - Visitor's email address. Mandatory.
	 * textarea:message:<label-translation-id> - Custom message. Mandatory.
	 * hidden:mailer:<auto-mailer-name> - Overrides the auto mailer name.
	 * hidden:mailerEmail:<auto-mailer-email> - Overrides the auto mailer email address.
	 * hidden:recipient:<website-owner's-name> - Overrides the recipient name of the contact request. Defaults to Mincko Support.
	 * hidden:recipientEmail:<website-owner's-email> - Overrides the recipient email of the contact request. Defaults to info@mincko.com.
	 * hidden:subject:<subject-for-email-to-website-owner> - Overrides the subject of the contact request email. 
	 * hidden:acknowledgeVisitor:<true/false> - If not specified defaults to true: Indicate if acknowledgement should be sent to visitor.
	 * hidden:ackSubjectLabel:<email-ack-subject-for-visitor-translation-id> - If not specified, default text will be used (EN/NL only)
	 * hidden:ackMessageLabel:<email-ack-message-for-visitor-translation-id> - If not specified, default text will be used (EN/NL only)
	 * hidden:doNoRedirect:<true/false> - Indicates whether or not to redirect after the contact request email is sent. Defaults to false.
	 */
	@Override
	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) {

		String html = "";

		String text = ParameterService.getParameter(requestContext, param[0]);
		String fieldsStr = ParameterService.getParameter(requestContext, param[1]);
		String language = ParameterService.getParameter(requestContext, param[2]);
		String submitButtonLabel = ParameterService.getParameter(requestContext, param[3]);
		
		if (language == null || language.isEmpty()) language = requestContext.getLanguage();
		
		// translate if labels and not actual texts
		text = getTranslation(language, text);
		submitButtonLabel = getTranslation(language, submitButtonLabel);

		html = "<div class=\"panel-body\" style=\"background-color:whitesmoke\">"
				+ "<div id=\"panelbody\">"
				+ ((text != null && !text.isEmpty()) ? ("<h3>" + text + "</h3>") : "") + "<br />"
				+ "<iframe class=\"hide\" id=\"iframe\"></iframe>"
				+ "<form action=\"/formsubmit\" role=\"form\" id=\"formTable\" enctype=\"multipart/form-data\" accept-charset=\"UTF-8\" method=\"get\" >";

		String[] fieldsA = fieldsStr.split(";");
		for (int i = 0; i < fieldsA.length; i++) {
			String[] field = fieldsA[i].split(":");
			String type = field[0];
			String name = field[1];
			String label = field[2];

			// If first character is lower case, it's most likely a label
			label = getTranslation(language, label);

			if ("text".equals(type) || "email".equals(type)) {
				html = html 
						+ "<div class=\"form-group\">"
						+ "  <label for=\"exampleInputEmail1\">" + label + " *</label>" 
						+ "  <input type=\"" + type + "\" class=\"form-control mandatory\" required id=\"" + name + "\" name=\"" + name + "\" placeholder=\""+ label + "\" />" 
						+ "  <input type=\"hidden\" id=\"" + name + "-label\" name=\"" + name + "-label\" value=\"" + label + "\" />" 
						+ "</div>";
			} else if ("textarea".equals(type)) {
				html = html
						+ "<div class=\"form-group\">" + "  <label for=\"exampleInputEmail1\">" + label + " *</label>"
						+ "	 <textarea class=\"form-control mandatory\" required id=\"" + name + "\" name=\"" + name + "\" placeholder=\"" + label + "\"></textarea>"
						+ "  <input type=\"hidden\" id=\"" + name + "-label\" name=\"" + name + "-label\" value=\"" + label + "\" />" 
						+ "</div>";
			} else if ("hidden".equals(type)) {
				html += "<input type=\"hidden\" id=\"" + name + "\" name=\"" + name + "\" value=\"" + field[2] + "\" />";
			}
		}

		html = html
				+ "	     <input type=\"hidden\" name=\"language\" value=\"" + language + "\">"
				+ "	     <div style=\"text-align: center\"><button id=\"btn-submit\" type=\"submit\" class=\"btn btn-danger\" >" + submitButtonLabel + "</button></div>" 
				+ "	   </form>"
				+ "	 </div>" 
				+ "</div>";

		ComponentCode cc = new ComponentCode();
		cc.setHtml(html);
		return cc;
	}

	/**
	 * Identify if labels are used instead of actual String, if so, take
	 * translation.
	 * 
	 * @param requestContext
	 * @param label
	 * @return
	 */
	public static String getTranslation(String language, String label) {
		if (Character.isLowerCase(label.charAt(0))) {
			String translated_label = TranslationService.translate(language,
					"form", label);
			if (!translated_label.contains(":form:"))
				label = translated_label;
		}

		return label;
	}

}
