package net.ginaapp.renderer.components.form;

import java.io.IOException;

import net.ginaap.controller.RequestContext;

public interface IFormAction {
	
	public void performAction( RequestContext requestContext) throws IOException;

}
