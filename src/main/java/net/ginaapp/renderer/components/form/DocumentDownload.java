package net.ginaapp.renderer.components.form;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.Person;
import net.ginaapp.services.OfyService;

import com.googlecode.objectify.Objectify;

/**
 * 
 * StorePerson IFormAction will store all Person information in a Person Object.
 * 
 * @author Lennart
 * 
 */
public class DocumentDownload implements IFormAction {

	@Override
	public void performAction(RequestContext requestContext) throws IOException{

		// Prepare RequestContext
		// RequestContext requestContext = new RequestContext(request,
		// response);
		HttpServletRequest request = requestContext.getRequest();

		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String company = request.getParameter("company");
		String language = request.getParameter("language");
		String formMessage = request.getParameter("message");

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		try {
			// mail to visitor
			String message;

			if ("nl".equals(language)) {
				message = "Beste "
						+ firstname
						+ ",<br><br>"
						+ "Bedankt voor uw interesse. U kan de white paper <a href=\"http://goo.gl/bs7GIK\">hier downloaden</a>.<br><br>"
						+ "Vriendelijke groeten,<br>"
						+ "Mincko team<br>"
						+ "info@mincko.com<br>" 
						+ "+32 473 550 133";
			} else {
				message = "Dear "
						+ firstname
						+ ",<br><br>"
						+ "Thank you for your interest. You can download your copy of the white paper  <a href=\"http://goo.gl/bs7GIK\">here</a>.<br><br>"
						+ "Best regards,<br>"
						+ "Mincko team<br>"
						+ "info@mincko.com<br>" 
						+ "+32 473 550 133";
			}

			Message msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress("no-reply@mincko.com", "Mincko"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					email, firstname + " " + lastname));

			msg.setSubject("Download link whitepaper");
			msg.setContent(message, "text/html");

			Transport.send(msg);

			// mail to mincko
			message = "Dear Minckonians,<br><br>"
					+ "Event: HHP white paper has been downloaded: <br>"
					+ "Firstname: " + firstname + "<br>" + "Lastname: "
					+ lastname + "<br>" + "Company: " + company + "<br>"
					+ "Email: " + email + "<br>" + "BR,<br>";

			msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress("no-reply@mincko.com", "Mincko"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"info@mincko.com", "Lennart Benoot"));

			msg.setSubject("HHP whitepaper downloaded");
			msg.setContent(message, "text/html");

			Transport.send(msg);
		}
		catch (Exception e) {
			//requestContext.getResponse().getOutputStream().println("error sending message" + e.getMessage());
		}
		
		requestContext.getResponse().sendRedirect("/"+ language +"/thanks-for-downloading");
		
	}

}
