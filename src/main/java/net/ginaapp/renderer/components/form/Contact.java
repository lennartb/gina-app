package net.ginaapp.renderer.components.form;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.RequestContext;
import net.ginaapp.services.ParameterService;
import net.ginaapp.services.TranslationService;

/**
 * 
 * StorePerson IFormAction will store all Person information in a Person Object.
 * 
 * @author Lennart
 * 
 */
public class Contact implements IFormAction {
	
	private static final Logger log = Logger.getLogger(Contact.class.getName());

	@Override
	public void performAction(RequestContext requestContext) throws IOException {
		
		Boolean doNotRedirect = false;
		String language = "nl";
		
		try {
			HttpServletRequest request = requestContext.getRequest();
			
			// Get form parameters
			String domain = "";
			if (request.getParameter("domain") == null || request.getParameter("domain").isEmpty()) {
				domain = requestContext.getRequest().getServerName();
			} else {
				domain = request.getParameter("domain");
			}
			
			int count = domain.length() - domain.replace(".", "").length();
			if (count == 2) domain = domain.substring(domain.indexOf(".") + 1, domain.length());
			
			language = ParameterService.getParameterValue(request, "language", String.class, "nl");
			doNotRedirect = ParameterService.getParameterValue(request, "doNoRedirect", Boolean.class, false);
			
			String firstname = ParameterService.getParameterValue(request, "firstname", String.class, "");
			String lastname = ParameterService.getParameterValue(request, "lastname", String.class, "");
			String sender = (firstname + " " + lastname).trim();
			String senderEmail = ParameterService.getParameterValue(request, "email", String.class, "");
			Boolean ackSender = ParameterService.getParameterValue(request, "acknowledgeVisitor", Boolean.class, true);
			String ackSubjectLabel = ParameterService.getParameterValue(request, "ackSubjectLabel", String.class, "ack-subject");
			String ackMessageLabel = ParameterService.getParameterValue(request, "ackMessageLabel", String.class, "ack-message");
			String mailer = ParameterService.getParameterValue(request, "mailer", String.class, "No Reply");
			String mailerEmail = ParameterService.getParameterValue(request, "mailerEmail", String.class, "no-reply@mincko.com");
			String recipient = ParameterService.getParameterValue(request, "recipient", String.class, "Mincko Support");
			String recipientEmail = ParameterService.getParameterValue(request, "recipientEmail", String.class, "info@mincko.com");
			String subject = ParameterService.getParameterValue(request, "subject", String.class, "");
			
			Message msg = null;
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);

			if (ackSender) {
				// Email a confirmation to the visitor (sender)
				ackSubjectLabel = getTranslation(language, ackSubjectLabel);
				if (ackSubjectLabel.equals("ack-subject")) {
					ackSubjectLabel = "Website " + domain + " Contact";
				}				
				
				ackSubjectLabel = ParameterService.replacePlaceholders(request, ackSubjectLabel);
				
				ackMessageLabel = getTranslation(language, ackMessageLabel);
				if (ackMessageLabel.equals("ack-message")) {
					if ("nl".equals(language)) {
						ackMessageLabel = "Beste "
								+ firstname
								+ ",<br><br>"
								+ "Bedankt voor uw intresse. Wij contacteren u zo spoedig mogelijk.<br><br>"
								+ "Vriendelijke groeten";
					} else {
						ackMessageLabel = "Dear "
								+ firstname
								+ ",<br><br>"
								+ "Thank you for your interest. We will get back to you as soon as possible.<br>"
								+ "Best regards";
					}
				}
				
				ackMessageLabel = ParameterService.replacePlaceholders(request, ackMessageLabel);
				
				msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(mailerEmail, mailer));
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(senderEmail, sender));
				msg.setSubject(ackSubjectLabel);
				msg.setContent(ackMessageLabel, "text/html");

				Transport.send(msg);
			}
			
			// Email website owner
			if (subject == null || subject.isEmpty()) {
				subject = getTranslation(language, "contact-details-subject");
				subject = ParameterService.replacePlaceholders(request, subject);
				if (subject.equals("contact-details-subject")) {
					subject = "Website " + domain + " Contact";
				}
			}
			
			String message = getTranslation(language, "contact-details-message");
			if (message == null || message.isEmpty() || message.equals("contact-details-message")) {
				message = "Hello,<br><br>"
					+ "A message from your website: <br> "
					+ "Firstname: {%firstname%}<br>" 
					+ "Lastname: {%lastname%}<br>"
					+ "Email: {%email%}<br>"
					+ "Message: {%message%}<br><br>" 
					+ "Best Regards";
			}
			
			message = ParameterService.replacePlaceholders(request, message);

			// Replace default message placeholders if no value found from the form
			message = message.replaceAll("\\{%firstname%\\}", "");
			message = message.replaceAll("\\{%lastname%\\}", "");
			message = message.replaceAll("\\{%email%\\}", "");
			message = message.replaceAll("\\{%message%\\}", "");
			
			msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailerEmail, mailer));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail, recipient));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress("info@"+ domain, "info@"+ domain));
			
			msg.setSubject(subject);
			msg.setContent(message, "text/html");
			
			Transport.send(msg);
			
			if (doNotRedirect) requestContext.getResponse().setStatus(HttpServletResponse.SC_OK);
			else requestContext.getResponse().sendRedirect("/" + language + "/thank-you");
		} catch (Exception e) {
			log.log(Level.SEVERE, "Unable send contact request email.", e);
			
			if (doNotRedirect) requestContext.getResponse().setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			else requestContext.getResponse().sendRedirect("/" + language + "/email-not-sent");
		}
	}
	
	private static String getTranslation(String language, String label) {
		if (label != null && !label.isEmpty() && Character.isLowerCase(label.charAt(0))) {
			String translated_label = TranslationService.translate(language, "form", label);
			if (!translated_label.contains(":form:")) label = translated_label;
		}

		return label;
	}

}
