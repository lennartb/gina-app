package net.ginaapp.renderer.components.form;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ginaap.controller.RequestContext;
import net.ginaapp.model.Person;
import net.ginaapp.services.OfyService;

import com.googlecode.objectify.Objectify;

/**
 * 
 * StorePerson IFormAction will store all Person information in a Person 
 * Object.
 * 
 * @author Lennart
 *
 */
public class StorePerson implements IFormAction {

	@Override
	public void performAction( RequestContext requestContext) throws IOException {
		
		// Get required parameters
		String firstname = requestContext.getRequest().getParameter("firstname");
		String lastname =requestContext.getRequest().getParameter("lastname");
		String email = requestContext.getRequest().getParameter("email");
		String company = requestContext.getRequest().getParameter("company");
		String language = requestContext.getRequest().getParameter("language");
		
		// Save to Person object
		Person person = new Person();
		person.setFirstname( firstname);
		person.setLastname( lastname);
		person.setEmail( email);
		person.setCompany( company);
		person.setChannel( "HHP Whitepaper");
		Objectify ofy = OfyService.ofy();
		ofy.put( person);
	}

}
