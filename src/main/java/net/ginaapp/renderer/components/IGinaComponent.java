package net.ginaapp.renderer.components;

import net.ginaap.controller.RequestContext;
import net.ginaapp.renderer.RenderException;

/**
 * 
 * Interface to be implemented by all GinaComponents
 * 
 * @author lennart.benoot@mincko.com
 *
 */
public interface IGinaComponent {

	public ComponentCode renderComponent(RequestContext requestContext,
			String[] param, String fragment) throws RenderException;

}
