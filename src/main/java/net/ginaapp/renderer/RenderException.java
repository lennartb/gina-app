package net.ginaapp.renderer;

public class RenderException extends Exception {
	
	  public RenderException() {
	  }

	  public RenderException(String msg) {
	    super(msg);
	  }
	  
	  public RenderException(String msg, Exception e) {
		    super(msg, e);
	   }

}
