package net.ginaapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import net.ginaapp.cli.CliUtil;

@Entity
public class Website {

	@Id
	Long id;
	private String googleDriveId;
	private String domain;
	private String template;
	private String defaultLanguage;
	private String defaultPage;

	/**
	 * Create objects from parameters. 1: domain example: www.gina-app.net 2:
	 * googleDriveId 3: template
	 * 
	 * @param param
	 */
	public void fromString(String param) {

		String[] p = param.split(",");

		if (p.length == 2) {
			
			this.domain = CliUtil.cleanParameter(p[0]);
			this.googleDriveId = CliUtil.cleanParameter(p[1]);
		} else if (p.length == 4) {

			this.domain = CliUtil.cleanParameter(p[0]);
			this.googleDriveId = CliUtil.cleanParameter(p[1]);
			this.defaultLanguage = CliUtil.cleanParameter(p[2]);
			this.defaultPage = CliUtil.cleanParameter(p[3]);
		} else if (p.length == 5) {
			
			this.domain = CliUtil.cleanParameter(p[0]);
			this.googleDriveId = CliUtil.cleanParameter(p[1]);
			this.defaultLanguage = CliUtil.cleanParameter(p[2]);
			this.defaultPage = CliUtil.cleanParameter(p[3]);
			this.template = CliUtil.cleanParameter(p[4]);
		}
	}

	public boolean isBasicWebsite() {
		
		if ( defaultPage == null) return true;
		if ( defaultPage.isEmpty()) return true;
				
		return false;
	}
	
	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getDefaultPage() {
		return defaultPage;
	}

	public void setDefaultPage(String defaultPage) {
		this.defaultPage = defaultPage;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoogleDriveId() {
		return googleDriveId;
	}

	public void setGoogleDriveId(String googleDriveId) {
		this.googleDriveId = googleDriveId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
