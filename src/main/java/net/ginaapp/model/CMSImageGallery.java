package net.ginaapp.model;

import java.util.ArrayList;

import com.google.gson.Gson;

public class CMSImageGallery {
	private ArrayList<CMSImageGalleryItem> items;
	
	public CMSImageGallery() {
		this.items = new ArrayList<CMSImageGalleryItem>();
	}

	public ArrayList<CMSImageGalleryItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<CMSImageGalleryItem> items) {
		this.items = items;
	}	
	
	public String toJson() {
		return new Gson().toJson(this);
	}
	
	public static CMSImageGallery fromJson(String json) {
		return new Gson().fromJson(json, CMSImageGallery.class);
	}
}
