package net.ginaapp.model;

import javax.persistence.Id;

import net.ginaapp.cli.CliUtil;

public class Translation {

	public @Id
	Long id;
	private String googleSheetId;

	public Translation() {
	}

	public Translation(String googleSheetId) {

		this.googleSheetId = googleSheetId;
	}

	public void fromString(String param) {

		String[] p = param.split(",");

		this.googleSheetId = CliUtil.cleanParameter(p[0]);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoogleSheetId() {
		return googleSheetId;
	}

	public void setGoogleSheetId(String googleSheetId) {
		this.googleSheetId = googleSheetId;
	}
}