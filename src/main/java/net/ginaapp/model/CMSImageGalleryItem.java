package net.ginaapp.model;

public class CMSImageGalleryItem {
	private String blobKey;
	private String title;
	private String description;
	
	public CMSImageGalleryItem() {
		
	}
	
	public CMSImageGalleryItem(String blobKey, String title, String description) {
		this.setBlobKey(blobKey);
		this.setTitle(title);
		this.setDescription(description);
	}
	
	public String getBlobKey() {
		return blobKey;
	}
	
	public void setBlobKey(String blobKey) {
		this.blobKey = blobKey;
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
