package net.ginaapp.model;

import com.google.gson.Gson;

public class CMSResponse {
	private boolean isSuccessful;
	private String errorMessage;
	private Object data;
	
	
	public boolean isSuccessful() {
		return isSuccessful;
	}
	
	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public String toJSON() {
		return new Gson().toJson(this);
	}
}
