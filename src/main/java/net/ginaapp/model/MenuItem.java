package net.ginaapp.model;

import javax.persistence.Id;

import net.ginaapp.cli.CliUtil;

public class MenuItem {

	public @Id
	Long id;
	private int menuorder;
	private String menuId;
	private String root;

	public MenuItem() {
	}

	public MenuItem(int menuorder, String menuId, String root) {

		this.menuorder = menuorder;
		this.menuId = menuId;
		this.root = root;
	}

	public void fromString(String param) {

		String[] p = param.split(",");

		String menuOrderStr = CliUtil.cleanParameter(p[0]);
		this.menuorder = Integer.parseInt(menuOrderStr);
		this.menuId = CliUtil.cleanParameter(p[1]);
		this.root = CliUtil.cleanParameter(p[2]);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMenuorder() {
		return menuorder;
	}

	public void setMenuorder(int menuorder) {
		this.menuorder = menuorder;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

}