package net.ginaapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import net.ginaapp.cli.CliUtil;

@Entity
public class Page {
	
    @Id Long id; 
    private String contentUrl;
    private String language;  /** can be a specific language such as en/fr/nl or * for language independent **/ 
    private String name;

    
    public void fromString( String param) {
    	String [] p = param.split(",");
  
    	this.language = CliUtil.cleanParameter( p[0]);
    	this.name = CliUtil.cleanParameter( p[1]);
    	this.contentUrl = CliUtil.cleanParameter( p[2]);
    }
    
    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getContentUrl() {
		return contentUrl;
	}


	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	

}
