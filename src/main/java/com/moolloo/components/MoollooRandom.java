package com.moolloo.components;

import java.util.Random;

import com.googlecode.objectify.Objectify;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.model.Page;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.OfyService;

/*
 * 
 * 
 */
public class MoollooRandom implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		Objectify ofy = OfyService.ofy();
		
		// Get the tag
		String pageName = requestContext.getPageName();
		int i = 0;
		while (!Character.isDigit(pageName.charAt(i))) i++;
		String tag = pageName.substring(0, i);
		
		// Find all pages with this tag
		int n = ofy.query(Page.class).filter("name >=", tag).filter("name <", tag + "\uFFFD").count();
		Random randomGenerator = new Random();
		int randomNumber = randomGenerator.nextInt( n) + 1;
		ComponentCode cc = new ComponentCode();
		cc.setHtml("<a href='/" + requestContext.getLanguage()+ "/"+tag+randomNumber+"'>Random</a>");
		
		return cc;
	}

}
