package com.moolloo.components;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;

/*
 * 
 * 
 */
public class MoollooSnippet implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		String googleDocsId = param[1].substring(1,
				param[1].length() - 1);
		String html = GinaUtil.readStyledGoogleDoc(googleDocsId);
		
		//Now replace all answers
		int i = 0;
		while ( html.contains( "&lt;answer&gt;")) {
			
			// little workaround with the TOREPLACE because exception if it is done directly in replaceFirst
			html = html.replaceFirst( "&lt;answer&gt;", "TOREPLACE" + i); 
			html = html.replace( "TOREPLACE" + i , "<div onClick='$( \"#answer"+i+"\" ).toggle();'>Show Answer</div><div id=\"answer"+i+"\" style=\"display:none\">");
			html = html.replaceFirst( "&lt;/answer&gt;", "</div>");
			i++;
		}
		
		html ="${com.moolloo.components.MoollooPrevious(\"\")} -- ${com.moolloo.components.MoollooNext(\"\")}" + "&nbsp" + "${com.moolloo.components.MoollooRandom(\"\")}<br><br>" + html;
		
		ComponentCode cc = new ComponentCode();
		cc.setHtml(html);
		
		return cc;
	}

}
