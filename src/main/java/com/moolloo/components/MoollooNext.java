package com.moolloo.components;

import java.util.Random;

import com.googlecode.objectify.Objectify;

import net.ginaap.controller.RequestContext;
import net.ginaapp.GinaUtil;
import net.ginaapp.model.Page;
import net.ginaapp.renderer.components.ComponentCode;
import net.ginaapp.renderer.components.IGinaComponent;
import net.ginaapp.services.OfyService;

/*
 * 
 *@author: Lennart Benoot
 */
public class MoollooNext implements IGinaComponent {

	@Override
	public ComponentCode renderComponent( RequestContext requestContext, String [] param, String fragment) {
		
		Objectify ofy = OfyService.ofy();
		
		// Get the tag
		String pageName = requestContext.getPageName();
		int i = 0;
		while (!Character.isDigit(pageName.charAt(i))) i++;
		String tag = pageName.substring(0, i);
		String idStr = pageName.substring(i, pageName.length());
		int id = Integer.valueOf( idStr);
		int n = ofy.query(Page.class).filter("name >=", tag).filter("name <", tag + "\uFFFD").count();
		
		int nextId; 
		if ( id == n)
			nextId=1;
		else
			nextId = id + 1;
		
		ComponentCode cc = new ComponentCode();
		cc.setHtml("<a href='/" + requestContext.getLanguage()+ "/"+tag+nextId+"'>Next &gt;&gt;</a>");
		
		return cc;
	}

}
