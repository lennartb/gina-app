<%@page import="com.googlecode.objectify.Query"%>
<%@page import="com.googlecode.objectify.Objectify"%>
<%@page import="net.ginaapp.services.OfyService"%>
<%@page import="net.ginaapp.model.Website"%>
<%@page import="net.ginaapp.services.TranslationService"%>
<%@page import="com.google.appengine.api.users.User"%>
<%@page import="com.google.appengine.api.users.UserService"%>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";
	String lang = (String) request.getAttribute("lang");
		
	String domain = "";	
	Objectify ofy = OfyService.ofy();
	Query<Website> q = ofy.query(Website.class);
	Website website = q.get();	
	if (website != null) {
		domain = website.getDomain();
	}
	
	String sender = "";
	String senderEmail = "";	
	UserService userService = UserServiceFactory.getUserService();
	User user = userService.getCurrentUser();
	if (user != null) {
		senderEmail = user.getEmail();
		
		sender = user.getNickname();
		sender = (sender.indexOf('.') >= 0) ? sender.substring(0, sender.indexOf('.')) : sender;
		sender = sender.substring(0, 1).toUpperCase() + sender.substring(1);
	}
%>

<html>
<head>
<title><%=TranslationService.translate(lang, "pages", "sbsp", SECUREX_TRANSLATIONS_RESX_ID)%></title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="./css/navbar-static-top.css" rel="stylesheet"
	type="text/css">
<link href="./css/gina.css" rel="stylesheet">
<link href="./css/custom.css" rel="stylesheet">
</head>

<body>

	<div class="header">
		<jsp:include page="header.jsp" />
	</div>

	<div class="container">
		<div class="jumbotron">
			<h1><%=TranslationService.translate(lang, "pages", "need-help", SECUREX_TRANSLATIONS_RESX_ID)%></h1>
			<h2><%=TranslationService.translate(lang, "pages",
					"happy-to-assist", SECUREX_TRANSLATIONS_RESX_ID)%></h2>
		</div>


		<h1><%=TranslationService.translate(lang, "pages",
					"support-form", SECUREX_TRANSLATIONS_RESX_ID)%></h1>
		<p>
		<ul class="nav nav-pills nav-stacked" style="width: 200px">
			<li class="active"><a href="#"> <span
					class="badge pull-right">12</span><%=TranslationService.translate(lang, "pages",
					"available-credits", SECUREX_TRANSLATIONS_RESX_ID)%>
			</a></li>
		</ul>
		</p>

		<form id="formTable" name="contactform" role="form"
			accept-charset="UTF-8" method="post" action="/formsubmit">

			<input type="hidden" name="language" class="form-control"
				value="<%=lang%>" /> <input type="hidden" name="domain"
				class="form-control " value="<%=domain%>" /> <input type="hidden"
				name="doNoRedirect" class="form-control" value="true" /> <input
				type="hidden" name="firstname" class="form-control"
				value="<%=sender%>" /> <input type="hidden" name="email"
				class="form-control " value="<%=senderEmail%>" /> <input
				type="hidden" name="recipient" class="form-control "
				value="Mincko Support" /> <input type="hidden" name="recipientEmail"
				class="form-control " value="info@mincko.com" /> <input
				type="hidden" name="message" class="form-control" />

			<div class="form-group">
				<label for="exampleInputEmail1"><%=TranslationService.translate(lang, "pages", "category", SECUREX_TRANSLATIONS_RESX_ID)%></label>
				<select class="form-control" name="categorie" id="categorie">
					<option value="1" selected><%=TranslationService.translate(lang, "pages", "website", SECUREX_TRANSLATIONS_RESX_ID)%></option>
					<option><%=TranslationService.translate(lang, "pages", "business", SECUREX_TRANSLATIONS_RESX_ID)%></option>
					<option><%=TranslationService.translate(lang, "pages", "invoicing", SECUREX_TRANSLATIONS_RESX_ID)%></option>
					<option><%=TranslationService.translate(lang, "pages",
					"time-and-attendence", SECUREX_TRANSLATIONS_RESX_ID)%></option>
					<option><%=TranslationService.translate(lang, "pages", "others", SECUREX_TRANSLATIONS_RESX_ID)%></option>
				</select>
				<p class="help-block" id="help-block"></p>
			</div>

			<div class="control-group">
				<label class="control-label"><%=TranslationService.translate(lang, "pages", "topic", SECUREX_TRANSLATIONS_RESX_ID)%></label>
				<div class="controls">
					<input type="text" name="subject" class="form-control mandatory"
						data-validation-required-message="<%=TranslationService.translate(lang, "pages", "required", SECUREX_TRANSLATIONS_RESX_ID)%>" />
					<p class="help-block" id="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><%=TranslationService
					.translate(lang, "pages", "description", SECUREX_TRANSLATIONS_RESX_ID)%></label>
				<div class="controls">
					<textarea name="description" class="form-control mandatory"
						data-validation-required-message="<%=TranslationService.translate(lang, "pages", "required", SECUREX_TRANSLATIONS_RESX_ID)%>" /></textarea>
					<p class="help-block" id="help-block"></p>
				</div>
			</div>
			<input id="btn-submit" type="submit"
				value="<%=TranslationService.translate(lang, "pages", "submit", SECUREX_TRANSLATIONS_RESX_ID)%>"
				class="btn btn-default" />
		</form>

		<div data-toggle="modal" class="modal fade" id="confirmService"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title"><%=TranslationService.translate(lang, "pages", "thank-you", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form">
							<fieldset>
								<!-- Text input-->
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p class="help-block"><%=TranslationService.translate(lang, "pages",
					"thank-you-for-contacting", SECUREX_TRANSLATIONS_RESX_ID)%></p>
										<input type="hidden" id="category-id">
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"
							id="btn-cancel"><%=TranslationService.translate(lang, "pages", "done", SECUREX_TRANSLATIONS_RESX_ID)%></button>
					</div>
				</div>
			</div>
		</div>

		<div data-toggle="modal" class="modal fade" id="errorService"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title"><%=TranslationService.translate(lang, "pages", "error", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form">
							<fieldset>
								<!-- Text input-->
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p class="help-block"><%=TranslationService.translate(lang, "pages",
					"error-sending-support-request", SECUREX_TRANSLATIONS_RESX_ID)%></p>
										<input type="hidden" id="category-id">
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"
							id="btn-cancel-error"><%=TranslationService.translate(lang, "pages", "done", SECUREX_TRANSLATIONS_RESX_ID)%></button>
					</div>
				</div>
			</div>
		</div>

		<br /> <br /> <br />

		<h1><%=TranslationService.translate(lang, "pages",
					"service-catalog", SECUREX_TRANSLATIONS_RESX_ID)%></h1>
		<div class="primary-wrapper">
			<div class="container primary-container">
				<div class="row primary">
					<%=TranslationService.translate(lang, "pages",
					"service-catalog-description-1", SECUREX_TRANSLATIONS_RESX_ID)%>
					<a target="_blank"
						href="https://docs.google.com/a/mincko.com/document/d/1PFu7w6AZFYpo4U0aoHBOQ1La7UnUZeuFSrYM62POGF8/edit"><%=TranslationService.translate(lang, "pages",
					"support-service-catalog", SECUREX_TRANSLATIONS_RESX_ID)%></a>
					<%=TranslationService.translate(lang, "pages",
					"service-catalog-description-2", SECUREX_TRANSLATIONS_RESX_ID)%>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 footer">
		<div class="container">
			<div class="col-md-12 footer-inner">
				<%=TranslationService.translate(lang, "pages",
					"in-cooperation-with", SECUREX_TRANSLATIONS_RESX_ID)%>
				<a href="http://www.mincko.com/"><%=TranslationService.translate(lang, "pages", "mincko", SECUREX_TRANSLATIONS_RESX_ID)%></a>
			</div>
		</div>
	</div>

	<script>
		$(function() {
			$("input,select,textarea").not("[type=submit]")
					.jqBootstrapValidation();
		});

		$("#btn-cancel").on("click", function() {
			$("#btn-submit").removeAttr("disabled");
			$("input[type=text],textarea").val("");
			$('#categorie > option[value="1"]').attr('selected', 'selected');
		});

		$("#btn-cancel-error").on("click", function() {
			$("#btn-submit").removeAttr("disabled");
		});

		$(document)
				.ready(
						function() {
							$('#formTable')
									.ajaxForm(
											{
												url : "/formsubmit",
												beforeSubmit : function(arr,
														$form, options) {
													var valid = !$("#formTable")
															.find(".mandatory")
															.jqBootstrapValidation(
																	"hasErrors");
													if (valid)
														valid = validateForm();
													if (!valid)
														return false;

													var msg = "Category:"
															+ $("#categorie")
																	.val()
															+ "<br />"
															+ "Subject:"
															+ $(
																	"input[name=subject]")
																	.val()
															+ "<br />"
															+ "Description:"
															+ $(
																	"textarea[name=description]")
																	.val()
															+ "<br />";
													$(
															"input[type=hidden][name=message]")
															.val(msg);

													$("#btn-submit").attr(
															"disabled",
															"disabled");
												},
												error : function() {
													$('#errorService').modal(
															'show');
												},
												success : function() {
													$('#confirmService').modal(
															'show');
												}
											});
						});
	</script>

	<script type="text/javascript">
		function validateForm() {
			var isValid = true;
			var name = "";
			var email = "";

			var subject = document.forms["contactform"]["subject"];
			isValid = !(subject.value == null || subject.value == "");

			var message = document.forms["contactform"]["description"];
			isValid = isValid
					&& !(message.value == null || message.value == "");

			return isValid;
		}
	</script>
	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="./js/jquery.form.min.js"></script>
	<script src="./js/jqBootstrapValidation.js"></script>
</body>
</html>