<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="net.ginaapp.services.TranslationService" %>

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";
	String lang = (String)request.getAttribute("lang"); 
%>

<html>
<head>
	<title><%=TranslationService.translate(lang, "pages", "sbsp", SECUREX_TRANSLATIONS_RESX_ID)%></title>
	<!-- Bootstrap core CSS -->
	<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
	<!-- Custom styles for this template -->
	<link href="./css/navbar-static-top.css" rel="stylesheet" type="text/css">
	<link href="./css/gina.css" rel="stylesheet">
</head>

<body>

	<!-- <div class="page"> -->
	<div class="header">
		<jsp:include page="header.jsp?pageName=home" />
	</div>
	
<%
	// Get user's first name
	UserService userService = UserServiceFactory.getUserService();
	User user = userService.getCurrentUser();
	if (user != null) {
		String username = user.getNickname();
		if(username.contains(".")){
		    username = username.substring( 0, username.indexOf('.'));
		}
		username = username.substring(0, 1).toUpperCase() + username.substring(1);
		pageContext.setAttribute("username", username);	
	}
%>

	<div class="container">
	    <div class="jumbotron">
		    <h1><%=TranslationService.translate(lang, "pages", "welcome", SECUREX_TRANSLATIONS_RESX_ID)%>, <%=pageContext.getAttribute("username")%></h1>
		    <h2><%=TranslationService.translate(lang, "pages", "to-your-service-portal", SECUREX_TRANSLATIONS_RESX_ID)%></h2>
	    </div>
	</div>

	<div class="primary-wrapper">
        <div class="container primary-container">
	        <div class="row primary">	        
				<div class="col-md-3 primary-button  " >	
					<h4 class="c0"><span><%=TranslationService.translate(lang, "pages", "start", SECUREX_TRANSLATIONS_RESX_ID)%></span></h4><p class="c3"><span class="c2"><%=TranslationService.translate(lang, "pages", "start-description", SECUREX_TRANSLATIONS_RESX_ID)%></span></p>
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1ky67HUSBKs_EgsKQ2PrbaJjeoSxwdJRK2pxE9Va_K8I/edit"><%=TranslationService.translate(lang, "pages", "general", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1WwF1cJ-UXc7V-qY8ss6W22i-5DTDEDkdyCBXTsS6TXA/edit"><%=TranslationService.translate(lang, "pages", "logo", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1fZtgQGy9GRGId8FhJTwKfjVPDz59jtT9HnTNJX1i2Wo/edit"><%=TranslationService.translate(lang, "pages", "business", SECUREX_TRANSLATIONS_RESX_ID)%></a> | 
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/19xQJB3NqFPF-IeqssrbW9tnOg8pCq0i1JVsh_a5diMQ/edit"><%=TranslationService.translate(lang, "pages", "website", SECUREX_TRANSLATIONS_RESX_ID)%></a>
	            </div>
	        
	            <div class="col-md-3 primary-button  " >	
					<h4 class="c0"><span><%=TranslationService.translate(lang, "pages", "info", SECUREX_TRANSLATIONS_RESX_ID)%></span></h4><p class="c3"><span class="c2"><%=TranslationService.translate(lang, "pages", "info-description", SECUREX_TRANSLATIONS_RESX_ID)%></span></p>
					<a href="learn.jsp"><%=TranslationService.translate(lang, "pages", "gmail", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a href="learn.jsp"><%=TranslationService.translate(lang, "pages", "update-website", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a href="learn.jsp"><%=TranslationService.translate(lang, "pages", "invoicing", SECUREX_TRANSLATIONS_RESX_ID)%></a>
	            </div>
	            
	            <div class="col-md-3 primary-button " >
					<h4 class="c1 c3"><span><%=TranslationService.translate(lang, "pages", "management", SECUREX_TRANSLATIONS_RESX_ID)%></span></h4><p class="c1"><span class="c0"><%=TranslationService.translate(lang, "pages", "management-description", SECUREX_TRANSLATIONS_RESX_ID)%></span></p>
					<a target="_blank" href="https://mail.google.com"><%=TranslationService.translate(lang, "pages", "e-mail", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://calendar.google.com"><%=TranslationService.translate(lang, "pages", "calendar", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://drive.google.com"><%=TranslationService.translate(lang, "pages", "documents", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://gapps.zoho.com/gappslogin?from=google&domain=blue-air.be&zservice=ZohoInvoice&gam=v2"><%=TranslationService.translate(lang, "pages", "customer-management", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a href="businessCard.jsp"><%=TranslationService.translate(lang, "pages", "order-business-cards", SECUREX_TRANSLATIONS_RESX_ID)%></a>
	            </div>
	            
	            <div class="col-md-3 primary-button " href="/en/mobileapps">
	                <h4 class="c0 c2"><span><%=TranslationService.translate(lang, "pages", "need-help", SECUREX_TRANSLATIONS_RESX_ID)%></span></h4><p class="c0"><span class="c1"><%=TranslationService.translate(lang, "pages", "need-help-description-1", SECUREX_TRANSLATIONS_RESX_ID)%> <a href="learn.jsp"><%=TranslationService.translate(lang, "pages", "info", SECUREX_TRANSLATIONS_RESX_ID)%></a> <%=TranslationService.translate(lang, "pages", "need-help-description-2", SECUREX_TRANSLATIONS_RESX_ID)%> <a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1tk3hfFjfVRlELrBgI5nwCJj4p2vWtx5qfXle4cjRpwI/edit#"><%=TranslationService.translate(lang, "pages", "faq", SECUREX_TRANSLATIONS_RESX_ID)%></a> <%=TranslationService.translate(lang, "pages", "need-help-description-3", SECUREX_TRANSLATIONS_RESX_ID)%></span></p>
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1tk3hfFjfVRlELrBgI5nwCJj4p2vWtx5qfXle4cjRpwI/edit#"><%=TranslationService.translate(lang, "pages", "faq", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a href="service.jsp"><%=TranslationService.translate(lang, "pages", "service-center", SECUREX_TRANSLATIONS_RESX_ID)%></a> |
					<a target="_blank" href="https://docs.google.com/a/mincko.com/document/d/1PFu7w6AZFYpo4U0aoHBOQ1La7UnUZeuFSrYM62POGF8/edit"><%=TranslationService.translate(lang, "pages", "credits-and-service-catalog", SECUREX_TRANSLATIONS_RESX_ID)%></a>
	            </div>	            
	        </div>
        </div>
        
        <div class="container primary-container">
	        <div class="row primary">
	            <div class="col-md-3 primary-button  " >	
	            	<div><img src="img/facebook-primary.png" style="margin-left:-7px"/></div>
	            	<br>
					<span><%=TranslationService.translate(lang, "pages", "keep-up-to-date-and-follow-on-facebook", SECUREX_TRANSLATIONS_RESX_ID)%></span>
					<div><a href="https://www.facebook.com/Ikstartmijnzaak" target="_blank"><%=TranslationService.translate(lang, "pages", "facebook", SECUREX_TRANSLATIONS_RESX_ID)%></a></div>
	            </div>
	            <div class="col-md-3 primary-button  " >	
	            	<div><img src="img/securex-primary.png" style="margin-left:-7px"/></div>
	            	<br>
					<span><%=TranslationService.translate(lang, "pages", "check-out-our-website", SECUREX_TRANSLATIONS_RESX_ID)%></span>
					<div><a href="http://www.securex.be/nl/zelfstandige" target="_blank"><%=TranslationService.translate(lang, "pages", "securex-starter", SECUREX_TRANSLATIONS_RESX_ID)%></a></div>
	            </div>
	            <div class="col-md-3 primary-button  " >	
	            	<div><img src="img/coach-primary.png" style="margin-left:-7px"/></div>
	            	<br>
					<span><%=TranslationService.translate(lang, "pages", "companion-to-start-your-website", SECUREX_TRANSLATIONS_RESX_ID)%></span>
					<div><a href="http://www.securex.be/shortcuts/starter-coach.html" target="_blank"><%=TranslationService.translate(lang, "pages", "starter-coach", SECUREX_TRANSLATIONS_RESX_ID)%></a></div>
	            </div>
	            <div class="col-md-3 primary-button  " >	
	            	<div><img src="img/brochures-primary.png" style="margin-left:-7px"/></div>
	            	<br>
					<span><%=TranslationService.translate(lang, "pages", "free-brochure-download", SECUREX_TRANSLATIONS_RESX_ID)%></span>
					<div><a href="http://www.securex.be/nl/groep/landingspages/brochures/" target="_blank"><%=TranslationService.translate(lang, "pages", "our-brochures", SECUREX_TRANSLATIONS_RESX_ID)%></a></div>
	            </div>
	        </div>
		</div>
	</div>
	
    <div class="col-md-12 footer">
	    <div class="container">
		    <div class="col-md-12 footer-inner">
				<%=TranslationService.translate(lang, "pages", "in-cooperation-with", SECUREX_TRANSLATIONS_RESX_ID)%> <a href="http://www.mincko.com/"><%=TranslationService.translate(lang, "pages", "mincko", SECUREX_TRANSLATIONS_RESX_ID)%></a>
		    </div>
	    </div>
    </div>

</body>
</html>