var noInternet = "Sorry, your internet connectivity is unstable. Please try again later.";
var retryAgain = true;
var retrySecond = 2000;
var keyStr = "ABCDEFGHIJKLMNOP" +
             "QRSTUVWXYZabcdef" +
             "ghijklmnopqrstuv" +
             "wxyz0123456789+/" +
             "=";

function encode64(input, escapeReq) {
   if(escapeReq)
      input = escape(input);
    
   var output = "";
   var chr1, chr2, chr3 = "";
   var enc1, enc2, enc3, enc4 = "";
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output +
         keyStr.charAt(enc1) +
         keyStr.charAt(enc2) +
         keyStr.charAt(enc3) +
         keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
   } while (i < input.length);

   return output;
}

function decode64(input) {
   var output = "";
   var chr1, chr2, chr3 = "";
   var enc1, enc2, enc3, enc4 = "";
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   var base64test = /[^A-Za-z0-9\+\/\=]/g;
   if (base64test.exec(input)) {
      alert("There were invalid base64 characters in the input text.\n" +
            "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
            "Expect errors in decoding.");
   }
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }

      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";

   } while (i < input.length);

   return unescape(output);
}


//general error method
function globalErrorMethod (){
  $.mobile.loading( 'hide');
    bubbles_alert("Connection to "+appName+" server failed, please try again later");
    //if(isMobile()) navigator.app.exitApp();
}

function showErrorMsg (msg){
  $.mobile.loading( 'hide');
    bubbles_alert(msg);
   //if(isMobile()) navigator.app.exitApp();
}



function bubbles_alert(error){
    $.mobile.loading( 'hide');
    if(!isMobile()){
      alert(error);
      return;
    }
    navigator.notification.alert(
        error,  // message
        function(){},         // nothing at this moment
        appName,            // title
        'OK'                  // buttonName
    );
}

//check if it is a mobile, normally used for development
function isMobile(){
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
}


function registerAjaxProgressBar() {
    // $(document).ajaxStart(function() {
    //    $.mobile.loading( 'show');
    // });
    
    // $(document).ajaxComplete(function(e, xhr, settings) {
    //      $.mobile.loading( 'hide');
    // });
  
}
function callServer(method,data, successMethod, errorMethod, noLoading){
  if(!checkConnection()){
    setTimeout(function(){
         if(checkConnection(true)){
            ajaxCall(method,data, successMethod, errorMethod, noLoading)
         }else
            $.mobile.loading("hide");

    },retrySecond)
  }else
   ajaxCall(method,data, successMethod, errorMethod, noLoading)
}

//generic function to interact with server
function ajaxCall(method,data, successMethod, errorMethod, noLoading){
    if(!noLoading) $.mobile.loading("show");
    $.ajax
    ({
        type: method,
        url: rootUrl,
        data: data,
        dataType: 'json',
        async: true,
        timeout:30000,
        success: function (response){
          retryAgain = true;
          if(response === null){
            if(typeof errorMethod === "function")
              errorMethod(response);
            else
              globalErrorMethod(response);
          }else{
              if(response.statuscode === 1){
                if(data.url ==="venue/findNear") errorMethod (response);
                else
                  showErrorMsg(response.message);
                return;
              }
               if(response.data !== null && typeof response.data === "string")
                   response.data = JSON.parse(response.data)
                successMethod(response)
          }
           
            
        },error: function(response){
           if(response && response.status === 0 
              && (response.statusText === "error" || response.statusText === "timeout"))  {
              if(retryAgain){
                  ajaxCall(method,data, successMethod, errorMethod, noLoading)
                  retryAgain = false
              }else{
                $.mobile.loading("show"); 
                bubbles_alert(noInternet);
              }
              return;
           }

           if(typeof errorMethod === "function")
              errorMethod(response);
            else
              globalErrorMethod(response);
        }
    }); 
}

//search json
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}


//get the current time
function getTime24(){
  var currentTime = new Date()
  var hours = currentTime.getHours()
  var minutes = currentTime.getMinutes()

  if (minutes < 10)
  minutes = "0" + minutes

  return hours + ":" + minutes;
}


//to verify email
function isEmail(email) {
  var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
  return pattern.test(email);
}


//calc birthday
function calcAge(dateString) {
  var birthday = +new Date(dateString);
  return ~~((Date.now() - birthday) / (31557600000));
}


//for ios 7, to check tweak is required or not
function tweakRequired(){
  if(!window.device) return false;

  return parseFloat(window.device.version) === 7.0
}

//for IOS7, the footer will appear in middle of the screen brieftly when there is a focus on input text. 
//In jquery.mobile.1-2.1.js, it will call this function and force it to true to hide the bar.
function tweakIOS7Footer(){
   return tweakRequired() && ($(".ui-page-active").attr("id") === "register" || $(".ui-page-active").attr("id") === "login_page")
}


function isAndriod(){
  var ua = navigator.userAgent.toLowerCase();
  return ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
}


function loadAndriodFiles(){
  $('<link rel="stylesheet" type="text/css" href="css/andriod.css" >').appendTo("head");
}


/*!
 * @param {number} duration - The speed amount
 * @param {string} easing - The easing method
 * @param {function} complete - A callback function
**/
jQuery.fn.slideLeftOut = function (duration, easing, complete) {
    return this.animate({
        left: -this.outerWidth()
    }, jQuery.speed(duration, easing, complete));
};

/*!
 * @param {number} duration - The speed amount
 * @param {string} easing - The easing method
 * @param {function} complete - A callback function
**/
jQuery.fn.slideLeftIn = function (duration, easing, complete) {
    return this.animate({
        left: 0
    }, jQuery.speed(duration, easing, complete));
};

/*!
 * @param {number} duration - The speed amount
 * @param {string} easing - The easing method
 * @param {function} complete - A callback function
**/
jQuery.fn.slideRightOut = function (duration, easing, complete) {
    return this.animate({
        right: -this.outerWidth()
    }, jQuery.speed(duration, easing, complete));
};

/*!
 * @param {number} duration - The speed amount
 * @param {string} easing - The easing method
 * @param {function} complete - A callback function
**/
jQuery.fn.slideRightIn = function (duration, easing, complete) {
    return this.animate({
        right: 0
    }, jQuery.speed(duration, easing, complete));
};


//for IOS7 issue
function tweakIOS7Css(){
    //$("div[data-role='page']").css("margin-top","20px");
    $("input[type='date']").css("height","30px")
}

//for Andriod specfic css issue
function tweakANDCss(){
    $("#order-header").css("top","20px");
}

function jDecode(str) {
    return $("<div/>").html(str).text();
}



function checkConnection(showErrorMsg){
  
  if(!isMobile()) return true;

  var networkState = navigator.connection.type;
  if(networkState === Connection.UNKNOWN || networkState === Connection.NONE){
    if(showErrorMsg) 
      bubbles_alert(noInternet)
    return false;
  }
  return true;
}


function hostReachable() {
 
  // Handle IE and more capable browsers
  var xhr = new ( window.ActiveXObject || XMLHttpRequest )( "Microsoft.XMLHTTP" );
  var status;
 
  // Open new request as a HEAD to the root hostname with a random param to bust the cache
  xhr.open( "HEAD", "//" + url + "/?rand=" + Math.floor((1 + Math.random()) * 0x10000), false );
 
  // Issue request and handle response
  try {
    xhr.send();
    return ( xhr.status >= 200 && xhr.status < 300 || xhr.status === 304 );
  } catch (error) {
    return false;
  }
 
}
