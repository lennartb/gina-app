<%@page import="com.googlecode.objectify.Query"%>
<%@page import="com.googlecode.objectify.Objectify"%>
<%@page import="net.ginaapp.services.OfyService"%>
<%@page import="net.ginaapp.model.Website"%>
<%@page import="net.ginaapp.services.TranslationService"%>

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";
	String lang = (String) request.getAttribute("lang");
%>

<html>

<head>
<title><%=TranslationService.translate(lang, "pages", "sbsp", SECUREX_TRANSLATIONS_RESX_ID)%></title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="./css/navbar-static-top.css" rel="stylesheet"
	type="text/css">
<link href="./css/gina.css" rel="stylesheet">
<link href="./css/custom.css" rel="stylesheet">
</head>

<body>

	<div class="header">
		<jsp:include page="header.jsp" />
	</div>

	<%
		Objectify ofy = OfyService.ofy();
		Query<Website> q = ofy.query(Website.class);
		Website website = q.get();
		String googleDriveId = website.getGoogleDriveId();
	%>

	<div class="container">
		<div class="primary-wrapper">
			<div class="container primary-container">
				<div class="row primary">
					<div class="card-container">
						<div class="left-container">
							<p><%=TranslationService.translate(lang, "pages", "front", SECUREX_TRANSLATIONS_RESX_ID)%></p>
							<img
								src="/img/business-card-front.png" />
						</div>
						<div class="right-container">
							<p><%=TranslationService.translate(lang, "pages", "back", SECUREX_TRANSLATIONS_RESX_ID)%></p>
							<img
								src="/img/business-card-back.png" />
						</div>
					</div>

					<form role="form" style="margin-left: 80px !important">
						<div class="form-group">
							<label for="exampleInputEmail1"><%=TranslationService.translate(lang, "pages", "number", SECUREX_TRANSLATIONS_RESX_ID)%></label>
							<select class="form-control" style="width: 200px;">
								<option>100</option>
								<option>200</option>
								<option>500</option>
								<option>1000</option>
							</select>
						</div>
						<button data-toggle="modal" href="#confirmMessage"
							class="btn btn-default" id="confirm1"><%=TranslationService.translate(lang, "pages", "confirm", SECUREX_TRANSLATIONS_RESX_ID)%></button>
					</form>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div data-toggle="modal" class="modal fade" id="confirmMessage"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title"><%=TranslationService.translate(lang, "pages",
					"confirmation", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form">
							<fieldset>
								<!-- Text input-->
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p class="help-block"><%=TranslationService.translate(lang, "pages",
					"confirm-order", SECUREX_TRANSLATIONS_RESX_ID)%></p>
										<input type="hidden" id="category-id">
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="modal-footer">
						<button data-toggle="modal" type="button" class="btn btn-primary"
							href="#confirmMessage2" id="btn-confirm"><%=TranslationService.translate(lang, "pages", "confirm", SECUREX_TRANSLATIONS_RESX_ID)%></button>
						<button type="button" class="btn btn-default" data-dismiss="modal"
							id="btn-cancel"><%=TranslationService.translate(lang, "pages", "cancel", SECUREX_TRANSLATIONS_RESX_ID)%></button>
					</div>
				</div>
			</div>
		</div>

		<div data-toggle="modal" class="modal fade" id="confirmMessage2"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title"><%=TranslationService.translate(lang, "pages", "thank-you", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form">
							<fieldset>
								<!-- Text input-->
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p class="help-block"><%=TranslationService.translate(lang, "pages",
					"order-confirmed", SECUREX_TRANSLATIONS_RESX_ID)%></p>
										<input type="hidden" id="category-id">
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"
							id="btn-cancel"><%=TranslationService.translate(lang, "pages", "done", SECUREX_TRANSLATIONS_RESX_ID)%></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 footer">
		<div class="container">
			<div class="col-md-12 footer-inner">
				<%=TranslationService.translate(lang, "pages",
					"in-cooperation-with", SECUREX_TRANSLATIONS_RESX_ID)%>
				<a href="http://www.mincko.com/"><%=TranslationService.translate(lang, "pages", "mincko", SECUREX_TRANSLATIONS_RESX_ID)%></a>
			</div>
		</div>
	</div>

	<script language="javascript">
		$("#btn-confirm").on("click", function() {
			$("#confirmMessage").modal('hide');
		})
	</script>
	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>