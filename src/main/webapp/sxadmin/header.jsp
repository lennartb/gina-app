
<%@ page import="com.googlecode.objectify.Objectify"%>
<%@ page import="com.googlecode.objectify.Query"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ page import="net.ginaapp.services.TranslationService"%>
<%@ page import="net.ginaapp.services.OfyService"%>
<%@ page import="net.ginaapp.model.Website"%>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template -->
<link href="./css/navbar-static-top.css" rel="stylesheet"
	type="text/css">
<link href="./css/gina.css" rel="stylesheet">

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";

	UserService userService = UserServiceFactory.getUserService();
	User user = userService.getCurrentUser();
	String userName = (user == null) ? null : user.toString();

	String openPub = request.getParameter("openPublic");
	String pageName = request.getParameter("pageName");
	String lang = (String) request.getAttribute("lang");

	String originalRequestedUrl = ((lang.isEmpty())
	? ""
	: ("/" + lang))
	+ request.getRequestURI()
	+ "?"
	+ request.getQueryString();

	String loginUrl = userService.createLoginURL(originalRequestedUrl);
	String logOffUrl = userService
	.createLogoutURL(originalRequestedUrl);

	boolean changeNamespace = false;
	String namespace = "";

	String title = TranslationService.translate(lang, "pages",
	"web-administration", SECUREX_TRANSLATIONS_RESX_ID);

	// Check for logged user
	boolean isAuth = false;
	if (user != null) {
		isAuth = userService.isUserLoggedIn();
	}

	// More authorization checks
	if (isAuth) {
		isAuth = false;
		
		String email = user.getEmail();
		String emailDomain = null;
		if (!email.isEmpty()) {
	String[] str = email.split("@");
	if (str.length == 2) emailDomain = str[1].toLowerCase();
		}
		
		// User's email must be part of the mincko team
		isAuth = emailDomain.contains("mincko.com") || emailDomain.contains("blue-air.be");
		
		
		// Or user's email is from the same domain as the website
		if (!isAuth) {
	Objectify ofy = OfyService.ofy();
	Query<Website> q = ofy.query(Website.class);
	Website website = q.get();
	
	if (website != null) {
		String domain = website.getDomain();
		if (domain != null) {
			isAuth = domain.toLowerCase().contains(emailDomain);
		}
	}
		}
	}

	if (!isAuth && openPub == null) {
%>

<script type="text/javascript">
	window.location.href = "<%=loginUrl%>"
</script>

<%
	} else if (!isAuth) {
		title = "";
	}
%>

<%
	if (openPub == null || isAuth) {
%>

<!-- Display navigation links if the user is authenticated and has the proper rights to view the current page. -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only"><%=TranslationService.translate(lang, "pages",
						"toggle-navigation", SECUREX_TRANSLATIONS_RESX_ID)%></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><img src="/img/portal-logo.png"></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="home.jsp"><%=TranslationService.translate(lang, "pages", "home", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a target="_blank" href="https://www.gmail.com"><%=TranslationService.translate(lang, "pages", "e-mail", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a target="_blank" href="https://calendar.google.com"><%=TranslationService.translate(lang, "pages", "calendar", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a target="_blank" href="https://drive.google.com"><%=TranslationService.translate(lang, "pages", "documents", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a target="_blank"
					href="https://gapps.zoho.com/gappslogin?from=google&domain=blue-air.be&zservice=ZohoInvoice&gam=v2"><%=TranslationService.translate(lang, "pages", "customer-management", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a href="/"><%=TranslationService
						.translate(lang, "pages", "website", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
				<li><a href="businessCard.jsp"><%=TranslationService.translate(lang, "pages", "business", SECUREX_TRANSLATIONS_RESX_ID)%></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<!--  <a href="#">Inbox <span class="badge">4</span></a>-->
				</li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<%
	} else {
		String content = "<div id='menu'>";
		content += "<div id='menu_container'>";
		content += "<ul><li><a href=\""
		+ loginUrl
		+ "\">"
		+ TranslationService.translate(lang, "pages",
				"login-with-google-apps", SECUREX_TRANSLATIONS_RESX_ID) + "</a></li></ul>";
		content += "</div></div>";
%>

<!-- Display the login link if the user is not authenticated. -->
<%=content%>

<%
	}
%>

</div>

<script type="text/javascript">	
	var namespace = "";
	var redirectMsg = "empty";
	
	function openAdminPage (url) {
		//namespace= "127.0.0.1:8888" //for testing
		if (namespace) {
			alert(redirectMsg);
			url = window.location.protocol + "//" + namespace + url;
		}
		
		window.location.href = url;
	}
	
	<%// put admin js code here.
			if (!namespace.isEmpty()) { // safer to put inside admin header for some specify code%>
	
		namespace = "<%=namespace%>";	
		redirectMsg = TranslationService.translate(lang, "pages", "redirect-message-1", SECUREX_TRANSLATIONS_RESX_ID) + 
			namespace + TranslationService.translate(lang, "pages", "redirect-message-2", SECUREX_TRANSLATIONS_RESX_ID);
		var serviceCaller = "/serviceCaller?ns=<%=namespace%>";
		var userURL = window.location.protocol + "//<%=namespace%>/";
		
		//testing
		//userURL = window.location.protocol+"//127.0.0.1:8888/"
		
		function getWebComp()
		{
			var serviceName = "GetWebComp"
			var jsonSend = JSON.stringify({ service: serviceName, param: ""});
			$.ajax({
				type: "POST",
				url: serviceCaller,
				data: { jsonData: jsonSend }
			}).done(function (json) {
				if(json) {
					var content = '<option value="" selected><%=TranslationService.translate(lang, "pages",
						"select-web-component", SECUREX_TRANSLATIONS_RESX_ID)%></option>';
					for(i = 0; i < json.length; ++i)
						content += '<option value="' + json[i].identifier + '">' + json[i].name + '</option>';
					}

					$('#webCompSelect').html(content);
			}).fail(function (data) {
				$('#returnMsg').html("<%=TranslationService.translate(lang, "pages", "error", SECUREX_TRANSLATIONS_RESX_ID)%>
	")
						});
	}

	$(document).ready(function() {
		getWebComp();
	});
<%}%>
	
</script>

<script src="./js/jquery-1.10.2.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8" src="js/fwk.js"></script>
<script>
	$(document).ready(function() {
		// Iterate through your links and see if they are found in the window.location.pathname string
		var loc_href = window.location.pathname.replace('/newAdmin/', '');
		$('.nav li').removeClass('active');
		$('.nav li a').each(function() {
			if (loc_href == $(this).attr('href')) {
				$(this).parent().addClass('active');
			}
		});
	});
</script>