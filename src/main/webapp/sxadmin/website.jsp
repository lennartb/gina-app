<%@page import="java.util.List"%>
<%@page import="net.ginaapp.services.TranslationService"%>

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";
	String content = "";
	String lang = (String) request.getAttribute("lang");
%>

<%!private String normalizeText(String str) {
		if (str == null)
			return "";

		// Obtain trailing numbers
		String numericPortion = "";
		int i = 0;
		for (i = (str.length() - 1); i >= 0; i--) {
			String c = str.substring(i, (i + 1));
			try {
				Integer.parseInt(c);
				numericPortion = c + numericPortion;
			} catch (NumberFormatException e) {
				break;
			}
		}

		// Obtain text portion
		String textPortion = str.substring(0, ++i);

		// Make first letter upper case
		if (textPortion.length() == 1) {
			textPortion = textPortion.toUpperCase();
		} else if (textPortion.length() > 1) {
			textPortion = Character.toUpperCase(textPortion.charAt(0))
					+ textPortion.substring(1);
		}

		return textPortion + " " + numericPortion;
	}%>

<html>

<head>
<title><%=TranslationService.translate(lang, "pages", "sbsp", SECUREX_TRANSLATIONS_RESX_ID)%></title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="./css/navbar-static-top.css" rel="stylesheet"
	type="text/css">
<link href="./css/gina.css" rel="stylesheet">
<link href="./css/custom.css" rel="stylesheet">
</head>

<body>

	<div class="header">
		<jsp:include page="header.jsp" />
	</div>

	<div class="container">
		<div class="page">
			<div class="content">
				<h1><%=TranslationService.translate(lang, "pages",
					"website-content", SECUREX_TRANSLATIONS_RESX_ID)%></h1>


				<%
					List<String> languages = TranslationService.getLanguages(null);
					List<String> labels = TranslationService.getLabelsByModule(null, "texts");

					if (languages != null && languages.size() > 0 && labels != null
							&& labels.size() > 0) {

						String table = "<table class='table table-bordered'>";

						// Build tbody
						String currentGroup = "";
						String generalGroup = "";
						String otherGroups = "";
						
						for (String label : labels) {
							String[] str = label.split("_");
							String group = "";
							String section = "";
							String row = "";
							
							if (str == null) continue;
							else if (str.length == 2) {
								group = normalizeText(str[0]);
								section = normalizeText(str[1]);
							} else if (str.length == 1) {
								section = normalizeText(str[0]);
							} else continue;
							
							if (!group.isEmpty() && !currentGroup.equals(group)) {
								row += "<tr>";
								row += "<td>" + group + "</td>";
								row += "<td>&nbsp;</td>";

								for (String l : languages) {
									row += "<td>" + l + "</td>";
								}

								row += "</tr>";
								currentGroup = group;
							}
							
							row += "<tr>";
							row += "<td>&nbsp;</td>";
							row += "<td>" + section + "</td>";

							int numberOfEmptyDocPointers = 0;
							for (String l : languages) {
								// Get values for group and section by language
								String value = TranslationService.translate(l.toLowerCase(), "texts", label);
								if (value.length() == 44) {
									row += "<td><a target='_blank' href='https://docs.google.com/document/d/"
											+ value + "/edit'>"
											+ TranslationService.translate(lang, "pages", "edit", SECUREX_TRANSLATIONS_RESX_ID)
											+ "</a></td>";	
								} else {
									numberOfEmptyDocPointers++;
									row += "<td>&nbsp;</td>";
								}
							}

							row += "</tr>";
							
							if (numberOfEmptyDocPointers < languages.size()) {
								if (group.isEmpty()) generalGroup += row;
								else otherGroups += row;
							}
						}
						
						if (!generalGroup.isEmpty()) {
							table += "<tr>";
							table += "<td>&nbsp;</td>";
							table += "<td>&nbsp;</td>";

							for (String l : languages) {
								table += "<td>" + l + "</td>";
							}

							table += "</tr>";							
							table += generalGroup;	
						}
						
						table += otherGroups;
						table += "</table>";
				%>
				<%=table%>
				<%
					} else {
				%>
				<h2>No content defined.</h2>
				<%
					}
				%>


				<br>
				<div class="centerbutton">
					<a target="_blank" href="/?refresh=true" class="css3button"><%=TranslationService.translate(lang, "pages",
					"update-website", SECUREX_TRANSLATIONS_RESX_ID)%>!</a>
				</div>
				<br>
			</div>
		</div>
	</div>


	<div class="col-md-12 footer">
		<div class="container">
			<div class="col-md-12 footer-inner">
				<%=TranslationService.translate(lang, "pages",
					"in-cooperation-with", SECUREX_TRANSLATIONS_RESX_ID)%>
				<a href="http://www.mincko.com/"><%=TranslationService.translate(lang, "pages", "mincko", SECUREX_TRANSLATIONS_RESX_ID)%></a>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./js/jquery-1.10.2.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
</body>
</html>