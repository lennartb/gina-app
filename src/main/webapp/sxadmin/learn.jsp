<%@page import="net.ginaapp.services.TranslationService"%>

<%
	final String SECUREX_TRANSLATIONS_RESX_ID = "1AyV04_kvy2oydhGsmK3RLJBriJB7USoqk66kieQwCv4";
	String lang = (String) request.getAttribute("lang");
%>

<html>

<head>
<title><%=TranslationService.translate(lang, "pages", "sbsp", SECUREX_TRANSLATIONS_RESX_ID)%></title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="./css/navbar-static-top.css" rel="stylesheet"
	type="text/css">
<link href="./css/gina.css" rel="stylesheet">
</head>

<body>

	<div class="header">
		<jsp:include page="header.jsp" />
	</div>

	<div class="container">
		<div class="jumbotron">
			<h1><%=TranslationService.translate(lang, "pages", "info", SECUREX_TRANSLATIONS_RESX_ID)%>.
			</h1>
			<h2><%=TranslationService.translate(lang, "pages",
					"learn-smoothly", SECUREX_TRANSLATIONS_RESX_ID)%></h2>
		</div>
	</div>

	<div class="primary-wrapper">
		<div class="container primary-container">
			<div class="row primary">
				<div class="col-md-4 primary-button  ">
					<h4><%=TranslationService.translate(lang, "pages",
					"gmail-google-apps", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					<p><%=TranslationService.translate(lang, "pages",
					"google-apps-basics", SECUREX_TRANSLATIONS_RESX_ID)%></p>
					<a target="_blank" href="http://gninjasapp.appspot.com/index.jsp"><%=TranslationService.translate(lang, "pages",
					"start-module", SECUREX_TRANSLATIONS_RESX_ID)%></a>
				</div>
				<div class="col-md-4 primary-button  ">
					<h4><%=TranslationService.translate(lang, "pages",
					"update-website", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					<p><%=TranslationService.translate(lang, "pages",
					"learn-update-website", SECUREX_TRANSLATIONS_RESX_ID)%></p>
					<a target="_blank"
						href="https://docs.google.com/a/mincko.com/document/d/1XO1SDCHU7BXW0M3FKPYCFisZBQ7J0ghIvDl3p1CrUw8/edit"><%=TranslationService.translate(lang, "pages",
					"start-module", SECUREX_TRANSLATIONS_RESX_ID)%></a>
				</div>

				<div class="col-md-4 primary-button  ">
					<h4><%=TranslationService.translate(lang, "pages", "invoicing", SECUREX_TRANSLATIONS_RESX_ID)%></h4>
					<p><%=TranslationService.translate(lang, "pages",
					"learn-improve-productivity", SECUREX_TRANSLATIONS_RESX_ID)%></p>
					<a target="_blank"
						href="https://www.zoho.com/invoice/invoices.html"><%=TranslationService.translate(lang, "pages",
					"start-module", SECUREX_TRANSLATIONS_RESX_ID)%></a>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 footer">
		<div class="container">
			<div class="col-md-12 footer-inner">
				<%=TranslationService.translate(lang, "pages",
					"in-cooperation-with", SECUREX_TRANSLATIONS_RESX_ID)%>
				<a href="http://www.mincko.com/"><%=TranslationService.translate(lang, "pages", "mincko", SECUREX_TRANSLATIONS_RESX_ID)%></a>
			</div>
		</div>
	</div>
</body>
</html>